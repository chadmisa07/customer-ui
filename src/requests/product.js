import axios from 'axios'
import { createMenuInterface } from './menuInterface'
import { toastr } from 'react-redux-toastr'
import cloudinary from 'cloudinary'
import * as api from '../utils/api'

cloudinary.config({ 
  cloud_name: 'dvarqi8oo', 
  api_key: '356916821992389', 
  api_secret: '8offWzpvLu-bIut3xcBxkLbOlbk' 
})

export function createProduct(data) {
  return function(dispatch) {
    cloudinary.uploader.upload(data.product.image_url, function(result) { 
      data.product.image_url = result.url
      axios.post(api.URL+"api/product-item/", data.product)
        .then(function (response) {
          toastr.success('Success', 'Product was created')
          data.product = response.data.id
          dispatch(createMenuInterface(data))
        })
        .catch(function (error) {
          toastr.error('Error', 'Error creating product')
        })
    })
  }  
}