import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import * as api from '../utils/api'

export function createMenuInterface(data) {
  return function(dispatch) {
    axios.post(api.URL+"api/menu-interface/", data)
      .then(function (response) {
        toastr.success('Success', 'Interface was created')
        dispatch({type: "MENU_INTERFACE_CREATED", payload: response.data})
      })
      .catch(function (error) {
        toastr.error('Error', 'Error creating interface')
      })
  }  
}