import axios from 'axios'
import * as api from '../utils/api'

export function getUser(username){
  return function(dispatch) {
    axios.get(api.URL+"api/user/?username=" + username)
      .then(function (response) {
        dispatch({type: "FETCHED_USER", payload: response.data.results[0]})
      })
      .catch(function (error) {
        console.log("nirequest?" + error)
      })
  }  
}

