import React from 'react'
import { Row, Thumbnail, ButtonGroup } from 'react-bootstrap'

const Generic = (props) => {
  return(
    <div>
      <Row className="font-color-blue margin-bottom-top">
      </Row>
      <div className="chatbox-border">
        <Row className="padding-top-15">
        <div>
          <Thumbnail src={props.survey.question_image} 
            className="fb-button">
            <h3 className="fb-title">{props.survey.question}</h3>
            <ButtonGroup vertical block>
              {props.answers}
            </ButtonGroup>
          </Thumbnail>
        </div>
        </Row>
      </div>
    </div>
  )
}

export default Generic
