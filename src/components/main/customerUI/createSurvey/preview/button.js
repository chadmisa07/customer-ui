import React from 'react'
import { Row } from 'react-bootstrap'

const Button = (props) => {
  const questions = props.previewQuestion.map((question, index) => {
    let answers = question.answers.map((answer, index) => {
      return(
        <div key={index} className="btn-response-button text-center pull-left">{answer.text}</div>
      )
    })
    return(
      <div key={index}>
        <Row className="padding-top-15">
          <div className="text-center pull-left">
            <div className="btn-response-text text-center pull-left">
             {question.text}
            </div>
            {answers}
          </div>
        </Row>
      </div>
    )
  })

  return(
    <div>
      <Row className="font-color-blue margin-bottom-top">
      </Row>
      <div className="chatbox-border">
        {questions}
      </div>
    </div>
  )
}

export default Button
