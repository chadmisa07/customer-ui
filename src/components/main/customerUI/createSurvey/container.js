import React from 'react'
import Header from '../commons/header/header'
import Sidebar from '../commons/sidebar/sidebar'
import Modal from './modal'
import { connect } from 'react-redux'
import { Grid, Row, Col } from 'react-bootstrap'


const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    task: state.createSurvey.task,
    questions: state.createSurvey.questions,
    survey: state.createSurvey.survey,
    surveys: state.createSurvey.surveys,
    tasks: state.createSurvey.tasks
  }
}

class ChatContainer extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      "platform":32,
      "name": "",
      "description": "", 
      "created_by": props.user.id,
      "updated_by": props.user.id,
      "questionModal": false,
      "openModal": this.openModal.bind(this),
      "closeModal": this.closeModal.bind(this)
    }
  }

  closeModal(){
    this.setState({questionModal:false})
  }

  openModal(){
    this.setState({questionModal:true})
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Header selected="createSurvey" titleLeft="Create Your Bot" titleRight="Logout your account "/>
        </Row>
        <Row className="createbot-form">
          <Sidebar selected="botCreation"/>
          <div className="page-content">
            <Row className="micro-top-padding">
              <Col md={12}>
                {this.props.children}
              </Col>
            </Row>
          </div>
        </Row>
        <Modal {...this.props}/>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(ChatContainer)