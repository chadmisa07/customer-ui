import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import { Row, Col, FormGroup, ControlLabel, FormControl } from 'react-bootstrap'
import { Table, Button, Glyphicon } from 'react-bootstrap'
import Modal from './modal'
import { toastr } from 'react-redux-toastr'

class CreateSurvey extends Component {
  constructor(props) {
    super(props)
    this.state = {
      "questionModal": false,
      "openModal": this.openModal.bind(this),
      "closeModal": this.closeModal.bind(this)
    }
  }

  closeModal(){
    this.setState({questionModal:false})
  }

  openModal(){
    this.setState({questionModal:true})
  }

  render(){
    return (
      <div>
        <Row className="micro-top-padding">
          <Col md={6}>
            <form>
              <FormGroup bsSize="small">
                <ControlLabel>Name</ControlLabel>
                <FormControl
                  type="text"
                  placeholder="Enter Name"
                />
              </FormGroup>
              <FormGroup bsSize="small">
                <ControlLabel>Description</ControlLabel>
                <FormControl
                  componentClass="textarea"
                  placeholder="Enter Description"
                />
              </FormGroup>
              <FormGroup bsSize="small">
                <ControlLabel>Image</ControlLabel>
                <FormControl
                  type="file"
                  label="Image"
                />
              </FormGroup>
            </form>
          </Col>
          <Col md={6}>
            <p className="pull-right">
              <Button 
                bsStyle="warning" 
                bsSize="xsmall"
                onClick={() => browserHistory.push('/bot/survey')}
              >
                <Glyphicon glyph="glyphicon glyphicon-step-backward" />
                {'\u00A0'}Go Back
              </Button>
              {'\u00A0'}
              <Button 
                bsStyle="primary" 
                bsSize="xsmall"
              >
                <Glyphicon glyph="glyphicon glyphicon-floppy-save" />
                {'\u00A0'}Save
              </Button>
            </p>
          </Col>
          <Col md={12}>
            <h5>Questions</h5>
            <Table striped bordered condensed hover >
              <thead>
                <tr>
                  <th>#</th>
                  <th>Question</th>
                  <th>Answers</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Hello is it me that you are looking for?</td>
                  <td>
                    <p>- Yes</p>
                    <p>- No</p>
                    <p>- Maybe</p>
                  </td>
                  <td className="text-center">
                    <br/>
                    <Button 
                      bsStyle="warning" 
                      bsSize="xsmall" 
                      onClick={this.openModal.bind(this)} > 
                      <Glyphicon glyph="glyphicon glyphicon-pencil" />
                    </Button>
                    {'\u00A0'}
                    <Button 
                      bsStyle="danger" 
                      bsSize="xsmall" > 
                      <Glyphicon glyph="glyphicon glyphicon-remove" />
                    </Button>
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Are you in?</td>
                  <td>
                    <p>- It's so much better</p>
                    <p>- When everyone is in, are you in?</p>
                    <p>- Ikimashou!!</p>
                  </td>
                  <td className="text-center">
                    <br/>
                    <Button 
                      bsStyle="warning" 
                      bsSize="xsmall" 
                      onClick={this.openModal.bind(this)} > 
                      <Glyphicon glyph="glyphicon glyphicon-pencil" />
                    </Button>
                    {'\u00A0'}
                    <Button 
                      bsStyle="danger" 
                      bsSize="xsmall" > 
                      <Glyphicon glyph="glyphicon glyphicon-remove" />
                    </Button>
                  </td>
                </tr>
                <tr className="text-center">
                  <td colSpan={5}>
                    <Button 
                      bsStyle="info" 
                      bsSize="xsmall"
                      onClick={this.openModal.bind(this)}
                    >
                      <Glyphicon glyph="glyphicon glyphicon-plus-sign" />
                      {'\u00A0'}Add Question
                    </Button>
                  </td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
        <Modal {...this.state} />
      </div>
    )
  }
}

export default CreateSurvey