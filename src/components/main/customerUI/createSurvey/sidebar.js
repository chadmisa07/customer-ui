import React, { Component } from 'react'
import { Link } from 'react-router'

class CreateSurveySidebar extends Component{
  render(){
    return (
      <div className="left-side-bar-2">
        <ul className="sidebar-2">
          <li className="sidebar-2">
            <Link 
              to="/bot/survey" 
              activeClassName="active-2"
              className="sidebar-2" >Chat</Link>
          </li>
          <li className="sidebar-2">
            <Link 
              to="/survey/button" 
              activeClassName="active-2"
              className="sidebar-2" >Button</Link>
          </li>
          <li className="sidebar-2">
            <Link 
              to="/survey/carousel" 
              activeClassName="active-2"
              className="sidebar-2" >Carousel</Link>
          </li>
          <li className="sidebar-2">
            <Link 
              to="/survey/list" 
              activeClassName="active-2"
              className="sidebar-2" >List</Link>
          </li>
        </ul>
      </div>
    )
  }
}

export default CreateSurveySidebar