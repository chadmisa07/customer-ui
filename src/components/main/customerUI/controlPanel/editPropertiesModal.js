import React from 'react'

const EditPropertiesModal = (props) => {
  return (
    <div className="modal fade" id="editBotPropertiesModal">
      <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title" id="myModalLabel">Edit Bot</h4>
            </div>
            <div className="modal-body">
              <div className="row">
                
                  <div className="col-lg-12">
                    <div className="form-group">
                      <label>Bot Name</label>
                      <input type="text" className="form-control" placeholder="Bot Name"/>
                    </div>
                    <div className="form-group">
                      <label>Bot Description</label>
                      <input type="text" className="form-control" placeholder="Bot Description"/>
                    </div>
                  </div>
              </div>
            </div>
            <div className="modal-footer">
              <button className="btn btn-sm btn-primary" data-dismiss="modal">Update</button>
              <button className="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>            
            </div>
        </div>
      </div>
    </div>
  )
}

export default EditPropertiesModal