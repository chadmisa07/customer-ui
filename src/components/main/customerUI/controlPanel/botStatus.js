import React from 'react'

const BotStatus = (props) => {

    return(
        <div className="panel panel-red">
            <div className="panel-heading">
                <i className="fa fa-cog fa-fw pull-right"></i>
                <i className="fa fa-question-circle-o fa-fw pull-right"></i>
                <b className="font-size-15">Status</b>
            </div>
            <div className="panel-body">
                <div className="row">
                    <div className="text-center">
                        {props.botStatus.running ? <h2 className="green">RUNNING</h2> : <h2 className="red">STOPPED</h2>}
                    </div>
                </div>
                <div className="row">
                    <center>
                        <label className="switch">
                          <input type="checkbox" data-toggle="modal" data-backdrop="static"
                             data-target={props.on?"#deployModal":""} checked={props.on} onChange={props.turnOn}/>
                          <div className="slider round"></div>
                        </label>
                    </center>
                </div>
                <div className="row text-center">
                    <b>{props.botStatus.running ? "Turn Off" : "Turn On"}</b>
                </div>
            </div>
        </div>
    )
}

export default BotStatus