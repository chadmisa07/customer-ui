import React from 'react'

const BotProperties = (props) => {

    return(
        <div className="panel panel-red">
            <div className="panel-heading ">
            <i className="fa fa-edit fa-fw pull-right" data-toggle="modal" data-backdrop="static"
                     data-target="#editBotPropertiesModal"></i>
                <b className="font-size-15">Properties</b>
            </div>
            <div className="panel-body">
                 <br/>
                <div className="row margin-left-10">
                    <b>Bot ID : {props.bot.bots[0].id}</b>
                </div>
                <div className="row margin-left-10">
                    <b>Bot Name : {props.bot.bots[0].name}</b>
                </div>
                <div className="row margin-left-10">
                    <b>Linked Page : {props.facebook.linkedPage.name}</b>
                </div><br/><br/>
            </div>
        </div>
    )
}

export default BotProperties