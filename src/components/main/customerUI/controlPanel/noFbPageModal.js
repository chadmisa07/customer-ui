import React from 'react'

const NoFbPageModal = (props) => {
  return (
    <div className="modal fade" id="noFacebookPageModal">
      <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title" id="myModalLabel">No Facebook Page Present</h4>
            </div>
            <div className="modal-body">
              <div className="row">
                <p>&nbsp;&nbsp;There are no Facebook pages linked yet. You need to link a Facebook page for your bot.</p>
                <div className="clear-10"></div>
              </div>
            </div>
            <div className="modal-footer">
                 <a href="https://www.facebook.com/v2.8/dialog/oauth?client_id=1038731512904671&
                      redirect_uri=https://cbot-tooling2.herokuapp.com/fb/&auth_type=rerequest&scope=email,
                      public_profile,manage_pages,pages_messaging,pages_messaging_subscriptions,publish_pages,publish_actions" 
              className="btn btn-success btn-sm">Connect With Facebook</a>              
          <button className="btn btn-sm btn-default" data-dismiss="modal">Deploy Later</button>
            </div>
        </div>
      </div>
    </div>
  )
}

export default NoFbPageModal