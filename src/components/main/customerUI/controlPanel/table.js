import React from 'react'

const Table = (props) =>{
    return (
      <div style={{marginTop: '20px'}}>
        <div className="well">
        <p className="light-blue-text"><b>Bot Information</b></p>
        <br/>
        <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
          <thead>
            <tr>
              <th className="text-center">Bot ID</th>
              <th className="text-center">Bot Name</th>
              <th className="text-center">Bot Description</th>
              <th className="text-center">Jobs Defined</th>
            </tr>
          </thead>
          <tbody>
            {
              !props.bots.length?
              <tr>
                <td colSpan="5" className="tac">No Bot found</td>
              </tr> : null
            }
            {props.botlist}
          </tbody>
        </table>
        </div>
      </div>
    )
}


Table.propTypes = {
  currentPage: React.PropTypes.number,
  pageNumbers: React.PropTypes.array,
  handleFirst: React.PropTypes.func,
  handlePrevious: React.PropTypes.func,
  handleNext: React.PropTypes.func,
  handleLast: React.PropTypes.func,
  botlist : React.PropTypes.array,
}

export default Table