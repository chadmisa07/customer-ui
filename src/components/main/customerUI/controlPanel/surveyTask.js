import React from 'react'
import { Link } from 'react-router'

const SurveyTask = (props) => {

    return(
        <div className="panel panel-yellow">
            <div className="panel-heading">
                <b className="font-size-15">Survey</b>
                <div className="pull-right">
                    <div className="btn-group">
                        <button type="button" className="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            Actions&nbsp;
                            <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu pull-right" role="menu">
                            <li>
                                <a href="#" data-toggle="modal" data-backdrop="static"
                                  data-target="#assignSurvey">Assign Suryey
                                </a>
                            </li>
                            <li><Link to="/control-panel/create-survey">Create Survey</Link></li>
                            <li><a href="#" data-toggle="modal" data-backdrop="static"
                                  data-target="#editSurveyModal">Edit Survey</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="panel-body">
                 <br/>
                <div className="row margin-left-10">
                    <b>Bot ID : 123456</b>
                </div>
                <div className="row margin-left-10">
                    <b>Bot Name : Auto Bot</b>
                </div>
                <div className="row margin-left-10">
                    <b>Date Started : 05-May-2017</b>
                </div>
                <div className="row margin-left-10">
                    <b>Date End : Not Set</b>
                </div><br/>
            </div>
        </div>
    )
}

export default SurveyTask