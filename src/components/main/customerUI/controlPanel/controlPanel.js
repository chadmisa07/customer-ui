import React from 'react'
import { connect } from 'react-redux'
import Navigation from '../commons/navigation'
import * as Request from '../requests/requests'
import { Tabs, Tab } from 'react-bootstrap'
import * as CPanel from './index'
import { toastr } from 'react-redux-toastr'
import setAuthorizationToken from '../../../../utils/setAuthorizationToken'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bots: state.bot.bots,
    botCount: state.bot.count, 
    user: state.user,
    tasks: state.task,
    sidebar: state.sidebar,
    botList: state.bot.bots,
    bot: state.bot,
    fb_pages: state.botAdministration.fb_pages.sort(function(a, b){return a.id - b.id}),
    facebook: state.facebook,
    botStatus: state.botStatus.botStatus
  }
}

class ControlPanel extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			"selected": 1,
			showEditModal: false,
	      	showDeleteModal: false,
		    id: 0,
		    name: "",
		    description: "",
		    platform: null,
		    taskName: "",
		    currentPage: 1,
		    displayNumber: 3,
		    pageNumber: 0, 
		    nameError: null,
		    descriptionError: null,
		    showAdditionalInfo: true,
		    currentTask: "0",
		    date: new Date(),
		    showAttachModal: false,
      		showLaunchModal:false,
      		showUndeployModal:false,
      		showEditPropertiesModal:false,
		}
	}

	handleSelect(key) {
	    this.setState({selected:key})
	}

	componentWillMount(){
		if(this.props.auth.access_token){
      		setAuthorizationToken(this.props.auth.access_token)
   		}
	    this.props.dispatch(Request.getBot(1))
	    this.props.dispatch(Request.getJobFlow(1))
    	this.props.dispatch(Request.getFBPages())
    	this.props.dispatch({type:"SET_CURRENT_URL", payload: this.props.location.pathname})
    	this.setState({on: this.props.botStatus.running})
	}

	componentDidMount() {
    	this.setState({selectedFbPage: this.props.fb_pages[0]})  
  	}

	// componentWillReceiveProps(nextProps){
	// 	console.log(JSON.stringify(nextProps))
	// 	this.setState({on: nextProps.botStatus.running})
	// }

	openAttachModal(index){
	    this.setState({showAttachModal: true})
	    this.setState({id: this.props.bots[index].id})
	    this.setState({name: this.props.bots[index].name})
	    this.setState({description: this.props.bots[index].description})
	    this.setState({platform: this.props.bots[index].platform})
	    this.setState({currentTask: 0})
	}

	closeAttachModal(){
	    this.setState({ showAttachModal: false })
	    this.setState({id: ""})
	    this.setState({name: ""})
	    this.setState({description: ""})
	    this.setState({platform: ""})
	    this.setState({taskName: ""})
	    this.setState({taskId: ""})
	    this.setState({currentTask: 0})
	}

	deployBot() {
	    let selectedFbPage = this.state.selectedFbPage
	    let data = {
	      id: selectedFbPage.id,
	      name: selectedFbPage.name,
	      description: "This is a page description",
	      page_token: selectedFbPage.page_token,
	      page_id: selectedFbPage.page_id,
	      messenger_link: "https://m.me/"+selectedFbPage.page_id,
	      bot: null,
	      created_by: this.props.user.id,
	      updated_by: this.props.user.id
	    }
	    this.setState({selectedFbPage:data})
	    this.props.dispatch(Request.deployBot(data, {bot:this.props.bot.bots[0].id}, this.props.auth.access_token))
	    this.props.dispatch({type:"SHOW_OVERLAY", payload: true, overlayText: "Deploying Bot. Please wait."})
	}

	turnOn(){
		if(this.state.on){
			this.stopBot()
			this.setState({on:!this.state.on})
		}
		else{
			this.setState({on:!this.state.on})
		}
	}

	stopBot() {
	    this.props.dispatch(Request.stopBot(this.props.bot.bots[0].id, this.props.auth.access_token))
	}

	openLaunchModal(id) {
	    this.setState({ selectedBot: id })
	    this.setState({ showLaunchModal: true })
	    
	    if(!this.state.selectedFbPage){
	      this.setState({selectedFbPage: this.props.fb_pages[0]})  
	    }
	}

	openPropertiesModal(){
		this.setState({showEditPropertiesModal: !this.state.showEditPropertiesModal})
	}

	handleNameChange = (event) => {
	    if(!event.target.value.replace(/\s/g, '').length){
	      this.setState({nameError:"error"})
	    }else{
	      this.setState({nameError:null})
	    }
	    this.setState({name: event.target.value})
	}

	handleDescriptionChange = (event) => {
	    if(!event.target.value.replace(/\s/g, '').length){
	      this.setState({descriptionError:"error"})
	    }else{
	      this.setState({descriptionError:null})
	    }
	    this.setState({description: event.target.value})
	}

	closeLaunchModal() {
    	this.setState({ showLaunchModal: false })
  	}

  	closeUndeployModal() {
    	this.setState({ showUndeployModal: false })
  	}

	openUndeployModal(id) {
	    this.setState({ selectedBot: id })
	    this.setState({ showUndeployModal: true })
	}

	openNoFbPageModal(){
	    this.setState({ showNoFbPageModal: true })
	}

	closeNoFbPageModal(){
	    this.setState({ showNoFbPageModal: false })
  	}

	taskHandlerChange = (e) => {
	    this.setState({currentTask: e.target.value})
	    this.setState({taskId:e.target.value})
	  }

	attach(){
	    if(this.state.currentTask!== 0){
	      let data = {
	              name:this.state.name,
	              description:this.state.description,
	              platform:this.state.platform,
	             }
	      this.props.dispatch(Request.updateBot(this.state.id,this.state.currentTask,data))
	      this.setState({ showEditModal: false })
	    }else{
	      toastr.error('Error', 'Please select a task to attach')
	    }
	    
	}

	handleOnChange = (e) => {
	    this.setState({[e.target.name]:e.target.value})
	}

	handleFBPageSelection = (e) => {
	    this.setState({selectedFbPage:this.props.fb_pages[e.target.value]})
	}

	turnOff(){
		this.setState({on: false})
	}

	// deployBot() {
	//     let selectedFbPage = this.state.selectedFbPage
	//     let data = {
	//       id: selectedFbPage.id,
	//       name: selectedFbPage.name,
	//       description: "This is a page description",
	//       page_token: selectedFbPage.page_token,
	//       page_id: selectedFbPage.page_id,
	//       messenger_link: "https://m.me/"+selectedFbPage.page_id,
	//       bot: null,
	//       created_by: this.props.user.id,
	//       updated_by: this.props.user.id
	//     }
	//     this.setState({selectedFbPage:data})
	//     this.props.dispatch(Request.deployBot(data, {bot:this.state.selectedBot}, this.props.auth.access_token))
	//     this.closeLaunchModal()
	//     this.props.dispatch({type:"SHOW_OVERLAY", payload: true, overlayText: "Deploying Bot. Please wait."})
	// }

	render(){

		let pages = this.props.fb_pages

	    let deployedBots = []
	    for(let i=0;i<pages.length;i++){
	      if (pages[i].bot!==null) {
	        deployedBots.push(pages[i].bot)
	      }
	    }

	    let allBots = this.props.botList
	    let undeployedBots = []
	    let deployedBotsFinal = []
	    for(let i=0;i<allBots.length;i++){
	      let flag = false
	      for (let j=0;j<deployedBots.length;j++) {
	        JSON.stringify(allBots[i])
	        if (allBots[i].id===deployedBots[j]) {
	          flag = true
	        }
	      }
	      if (!flag) {
	        undeployedBots.push(allBots[i])
	      } else {
	        deployedBotsFinal.push(allBots[i])
	      }
	    }

		undeployedBots = undeployedBots.map((bot, index) => {
	      return (
	        <tr key={index} className='text-center'>
	          <td className="tac"><a>{bot.id}</a></td>
	          <td className="tac">{bot.name}</td>
	          <td className="tac">{bot.description}</td>
	          <td className="tac">{bot.task!==null?bot.task.name:""}</td>
	          <td className="tac">{bot.task===null? "Waiting for task": "Waiting for deployment"}</td>
	          <td className="tac">
	            {
	              bot.task===null?
	                <button 
	                  className="btn btn-success btn-sm"
	                  data-toggle="modal" data-backdrop="static"
	                  onClick={this.openAttachModal.bind(this,index)}
	                  data-target="#attachTaskModal" type="button">
	                    Attach Task
	                </button>
	              :this.props.fb_pages.length===0?
	                <button 
	                  className="btn btn-primary btn-sm"
	                  data-toggle="modal" data-backdrop="static"
	                  onClick={this.openNoFbPageModal.bind(this)}
	                  data-target="#noFacebookPageModal" type="button">
	                    Test & Launch
	                </button>
	                :
	                <button className="btn btn-primary btn-sm"
	                  data-toggle="modal" data-backdrop="static" 
	                  onClick={this.openLaunchModal.bind(this, bot.id)}
	                  data-target="#deployModal" type="button">
	                    Test & Launch
	                </button>
	            }
	          </td>
	        </tr>
	      )
	    })

		const fb_pages = this.props.fb_pages.map((fb_page, index) => {
	      return (<option value={index} key={index}>{fb_page.name}</option>)
	    })


		return (
			<div id="wrapper">
			
				<Navigation user={this.props.user.first_name}/>
      			<div id="myNav" className={this.props.bot.showOverlay? "overlay fullheight":"overlay clearheight"}>
		          <div className="overlay-content">
		            <div className="loader"></div><br/><br/>
		            <div className="margin-top-5">{this.props.bot.overlayText}</div>
		          </div>
		        </div>
		        <div id="page-wrapper">
		        	<div className="row">
		        	 	<h1 className="page-header">Control Panel</h1>
		            </div>
		            <div className="row">
			            <Tabs activeKey={ this.state.selected } onSelect={this.handleSelect.bind(this)} id="survey-details">
	                      	<Tab eventKey={1} title="Bot">
	                        	<div className="row top-padding-20">
	                        		<div className="col-lg-3">
	                        			<CPanel.EditPropertiesModal openPropertiesModal={this.openPropertiesModal.bind(this)}/>
	                        			<CPanel.BotProperties {...this.props}/>
	                        		</div>
	                        		<div className="col-lg-3">
		                        		<CPanel.DeployModal 
		                        			{...this.props} 
					                    	fbPages={fb_pages}
					                    	handleFBPageSelection={this.handleFBPageSelection.bind(this)}
					                    	deployBot={this.deployBot.bind(this)}
					                    	turnOff={this.turnOff.bind(this)}/>
	                        			<CPanel.BotStatus turnOn={this.turnOn.bind(this)} {...this.state} {...this.props}/>
	                        		</div>
	                        	</div>
	                      	</Tab>
	                      	<Tab eventKey={2} title="Task">
	                      		<div className="row top-padding-20">
	                        		<div className="col-lg-3">
	                        			<CPanel.SurveyTask {...this.props}/>
	                        		</div>
	                        		<div className="col-lg-3">
	                        			<CPanel.OrderTask />
	                        		</div>
	                        		<div className="col-lg-3">
	                        			<CPanel.FAQTask />
	                        		</div>
	                        		<CPanel.AssignSurveyModal openNoFbPageModal={this.openNoFbPageModal.bind(this)}/>
	                        		<CPanel.EditSurveyModal />
	                        	</div>
	                      	</Tab>  
	                    </Tabs>
		            </div>
		        </div>
		    </div>
		)

	}

}

export default connect(mapStateToProps)(ControlPanel)