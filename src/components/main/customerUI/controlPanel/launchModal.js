import React from 'react'

const LaunchModal = (props) => {
  return (
    <div className="modal fade" id="deployModal">
        <div className="modal-dialog">
          <div className="modal-content">
              <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 className="modal-title" id="myModalLabel">Deploy Bot</h4>
              </div>
              <div className="modal-body">
                <div className="row">
                  <p>&nbsp;&nbsp;&nbsp;<b>Select what Facebook page the attach to bot</b></p>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <select className="form-control" onChange={props.handleFBPageSelection} >
                      {props.fb_pages}
                    </select>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                   <button type="button" className="btn btn-success btn-sm" onClick={props.deployBot} 
                    data-dismiss="modal">Deploy
                  </button>
                  <button type="button" className="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
              </div>
          </div>
      </div>
    </div>
  )
}

export default LaunchModal