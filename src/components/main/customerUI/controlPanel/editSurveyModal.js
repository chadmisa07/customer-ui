import React from 'react'

const EditSurveyModal = (props) => {
  return (
   <div className="modal fade" id="editSurveyModal">
      <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title" id="myModalLabel">Edit Survey</h4>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-12">
                  <p><b>Select what Survey Task to edit</b></p>
                  <select className="form-control">
                    <option>Survey 1</option>
                    <option>Survey 2</option>
                    <option>Survey 3</option>
                  </select>
                  <div className="clear-10"></div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button className="btn btn-sm btn-primary" data-dismiss="modal">Edit</button>
              <button className="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>            
            </div>
        </div>
      </div>
    </div>
  )
}

export default EditSurveyModal