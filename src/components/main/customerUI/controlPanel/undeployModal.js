import React from 'react'

const UndeployModal = (props) => {
  return (
    <div className="modal fade" id="unDeployModal">
      <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title" id="myModalLabel">Undeploy Bot</h4>
            </div>
            <div className="modal-body">
              <h5>Are you sure you want to stop this bot?</h5>
              <div className="clear-10"></div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-danger btn-sm" onClick={props.stopBot} 
                data-dismiss="modal">Stop
              </button>
              <button type="button" className="btn btn-sm btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>
  )
}

export default UndeployModal