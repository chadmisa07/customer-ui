import React from 'react'

const AssignSurveyModal = (props) => {
  return (
    <div className="modal fade" id="assignSurvey">
      <div className="modal-dialog modal-sm">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title" id="myModalLabel">Assign Survey to Bot</h4>
            </div>
            <div className="modal-body">
              <div className="row text-center">
                <center>
                  <div className="col-lg-12">
                    <select className="form-control">
                      <option>Sample Bot 1</option>
                      <option>Sample Bot 2</option>
                    </select>
                  </div>
                </center>
              </div>
            </div>
            <div className="modal-footer">
              <button className="btn btn-sm btn-primary" data-dismiss="modal">Assign</button>
              <button className="btn btn-sm btn-danger" data-dismiss="modal">Remove</button>            
            </div>
        </div>
      </div>
    </div>
  )
}

export default AssignSurveyModal