import React from 'react'
import { connect } from 'react-redux'
import Navigation from '../commons/navigation'
import * as DBoard from './index'
import * as Request from '../requests/requests'
import setAuthorizationToken from '../../../../utils/setAuthorizationToken'
import { browserHistory } from 'react-router'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    surveyDetails: state.survey.surveyDetails,
    bot: state.bot,
    user: state.user,
    sidebar: state.sidebar,
    botStatus: state.botStatus.botStatus,
    fb_pages: state.botAdministration.fb_pages.sort(function(a, b){return a.id - b.id}),
    surveys: state.survey.surveys
  }
}

class DashBoard extends React.Component{
	constructor(props){
		super(props)
		this.state = {
			pieChartData : [],
			totalAnswer: 0,
			question: "",
			surveyName: "",
			surveyDetails: {},
			deployStatus: true,
		}
	}

	componentWillMount(){
		if(this.props.auth.access_token){
      		setAuthorizationToken(this.props.auth.access_token)
    	}
    	this.props.dispatch(Request.getFBPages())
    	this.props.dispatch(Request.getUserSurvey())
		this.props.dispatch(Request.getBotStatus(this.props.bot.bots.length?this.props.bot.bots[0].id:""))
		this.props.dispatch(Request.getSurveyDetails(this.props.bot.bots.length?this.props.bot.bots[0].id:""))
		this.props.dispatch({type:"SET_CURRENT_URL", payload: this.props.location.pathname})

	}

	componentDidMount() {
    	this.setState({selectedFbPage: this.props.fb_pages[0]})  
  	}

	componentWillReceiveProps(nextProps){
		// console.log(JSON.stringify(nextProps))

		if(JSON.stringify(nextProps.botStatus) !== "{}"){
			if(this.props.botStatus.faq > this.props.botStatus.survey && this.props.botStatus.faq > this.props.botStatus.order){
				this.setState({percentage: this.props.botStatus.faq.toFixed(2), task: "FAQ"})
			}else if(this.props.botStatus.survey > this.props.botStatus.faq && this.props.botStatus.survey > this.props.botStatus.order){
				this.setState({percentage: this.props.botStatus.survey.toFixed(2), task: "SURVEY"})
			}else if(this.props.botStatus.order > this.props.botStatus.faq && this.props.botStatus.order > this.props.botStatus.survey){
				this.setState({percentage: this.props.botStatus.order.toFixed(2), task: "ORDER"})
			}else{
				this.setState({percentage: 0, task: "None"})
			}
		}

		if(JSON.stringify(nextProps.surveyDetails) !== "{}"){
			
			let total = 0
			nextProps.surveyDetails.questions.map( (answer, index) =>{
				return(
					total+=answer.total_answers
				)
			})
			this.setState({totalAnswer : total})

			let data = [["Survey","Total Answer"]]
			nextProps.surveyDetails.questions[0].answers.map( (answer,index) =>{
			 	return(
			 		data.push([answer.text,answer.count])
			 	)
		 	})

		 	this.setState({pieChartData: data})
		 	this.setState({currentSurvey: nextProps.surveys[0].id})
			this.setState({question : nextProps.surveyDetails.questions[0].text})
			this.setState({surveyName: nextProps.surveyDetails.survey})
			this.setState({surveyDetails: nextProps.surveyDetails})
		}

	}

	deployBot() {
	    let selectedFbPage = this.state.selectedFbPage
	    let data = {
	      id: selectedFbPage.id,
	      name: selectedFbPage.name,
	      description: "This is a page description",
	      page_token: selectedFbPage.page_token,
	      page_id: selectedFbPage.page_id,
	      messenger_link: "https://m.me/"+selectedFbPage.page_id,
	      bot: null,
	      created_by: this.props.user.id,
	      updated_by: this.props.user.id
	    }
	    this.setState({selectedFbPage:data})
	    this.props.dispatch(Request.deployBot(data, {bot:this.props.bot.bots[0].id}, this.props.auth.access_token))
	    this.props.dispatch({type:"SHOW_OVERLAY", payload: true, overlayText: "Deploying Bot. Please wait."})
	}

	handleFBPageSelection = (e) => {
	    this.setState({selectedFbPage:this.props.fb_pages[e.target.value]})
	}


    taskHandlerChange = (e) => {
	    this.setState({currentSurvey: e.target.value})
	    this.setState({taskId:e.target.value})
	}

	redirectToCreateSurvey(){
		browserHistory.push("/control-panel/create-survey")
	}

	attachSurveyTask(){
		this.props.dispatch(Request.attachTaskToSurvey(this.state.currentSurvey, this.props.bot.bots[0].task.id, this.props.bot.bots[0].id))
	}

	render(){
		const fb_pages = this.props.fb_pages.map((fb_page, index) => {
	      return (<option value={index} key={index}>{fb_page.name}</option>)
	    })

	    const surveys = this.props.surveys.map((survey, index) => {
	      return (<option value={survey.id} key={index}>{survey.name}</option>)
	    })
		
		return (
			<div id="wrapper">

				<Navigation user={this.props.user.first_name}/>
      			<div id="myNav" className={this.props.bot.showOverlay? "overlay fullheight":"overlay clearheight"}>
		          <div className="overlay-content">
		            <div className="loader"></div><br/><br/>
		            <div className="margin-top-5">{this.props.bot.overlayText}</div>
		          </div>
		        </div>
		        <div id="page-wrapper">
		            <div className="row">
		                <div className="col-lg-12">
		                    <h1 className="page-header">Dashboard </h1>
		                </div>
		            </div>
		            <div className="row">
		                <div className="col-lg-3 col-md-6">
				            <DBoard.BotStatus {...this.props}/>
				        </div>
		                <div className="col-lg-3 col-md-6">
		                   <DBoard.InteractedUser users={this.props.botStatus.users_reached} {...this.props}/>
		                </div>
		                <div className="col-lg-3 col-md-6">
		                    <DBoard.MostInteracted {...this.state} {...this.props}/>
		                </div>
		                <div className="col-lg-3 col-md-6">
		                  <DBoard.BotSchedule {...this.props}/>
		                </div>
		            </div>
		            <div className="row">
		                <div className="col-lg-7">
		                    <DBoard.Insights surveyDetails={this.state.surveyDetails} 
		                    	title={this.state.question} 
		                    	name={this.state.surveyName} 
		                    	total={this.state.totalAnswer} 
		                    	pieChartData={this.state.pieChartData} 
		                    	{...this.props}/>
		                </div>
		                <div className="col-lg-5">
		                   	<DBoard.Engagement {...this.props}/>
		                    <DBoard.Care {...this.props}/>
		                    <DBoard.DeployModal 
		                    	{...this.props} 
		                    	fbPages={fb_pages}
		                    	handleFBPageSelection={this.handleFBPageSelection.bind(this)}
		                    	deployBot={this.deployBot.bind(this)}/>
		                    <DBoard.AttachTaskModal surveysOptions={surveys} 
		                    	{...this.props} 
		                    	redirectToCreateSurvey={this.redirectToCreateSurvey.bind(this)}
		                    	taskHandlerChange={this.taskHandlerChange.bind(this)}
		                    	attachSurveyTask={this.attachSurveyTask.bind(this)}/>
		                </div>
		            </div>
		        </div>
		    </div>
		)

	}

}

export default connect(mapStateToProps)(DashBoard)