import React from 'react'

const BotSchedule = (props) => {

    return(
        <div className={props.botStatus.running ? "panel panel-red" : "panel border-red"}>
            <div className="panel-heading">
                <div className="row">
                    <div className="col-xs-3">
                        <i className={!props.botStatus.running ? "red fa fa-clock-o fa-5x":"fa fa-clock-o fa-5x"}></i>
                    </div>
                    <div className="col-xs-9 text-right">
                        <div className={!props.botStatus.running ? "red font-17":"font-17"}>Bot Scheduled End</div>
                        <div className={!props.botStatus.running ? "red huge":"huge"}>Coming Soon</div>
                        <div><u><a href="#" className={!props.botStatus.running ? "red":""}>Extend</a></u></div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div className="panel-footer">
                    <span className={!props.botStatus.running ? "red pull-left":"pull-left"}>View Details</span>
                    <span className="pull-right"><i className={!props.botStatus.running ? "red fa fa-arrow-circle-right":"fa fa-arrow-circle-right"}></i></span>
                    <div className="clearfix"></div>
                </div>
            </a>
        </div>
    )
}

export default BotSchedule