import React from 'react'

const PurchaseModal = (props) => {
  let user = {}
  let bot = {}
  let items 
  let created_at
  let total = 0.0
  let statusArray = ["Received","Confirmed","Shipped","Delivered"]

  try {
    user = props.purchase.user
    bot = props.purchase.bot
    created_at = props.purchase.created_at
    items = props.purchase.items.map((item, index) => {
      total += (item.product.price * item.quantity)
      return(
        <div key={index}>
          {item.quantity + " pcs. " + item.product.name + " ₱" + (item.product.price * item.quantity)}
        </div>
      )
    })
  } catch(error) {
  }

  let status = statusArray.map((status, index) => {
      return (
        <option key={index} value={status}>{status}</option>
      )
    })

  return (
     <div className="modal fade" id="showDetailsModal">
      <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title" id="myModalLabel">Purchase Details</h4>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="row">
                  <div className="col-md-5">
                    <p className="pull-right"><b>Status:</b></p>
                  </div>
                  <div className="col-md-3">
                    <select value={props.currentStatus} onChange={props.handleChange} 
                      className="form-control margin-top-neg-6">
                        {status}
                    </select>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <p className="pull-right"><b>Bot:</b></p>
                  </div>
                  <div className="col-md-6">
                    <p className="pull-left"><b>{bot.name}</b></p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <p className="pull-right"><b>Description:</b></p>
                  </div>
                  <div className="col-md-6">
                    <p className="pull-left"><b>{bot.description}</b></p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <p className="pull-right"><b>Buyer:</b></p>
                  </div>
                  <div className="col-md-6">
                    <p className="pull-left"><b>{user.first_name+" "+user.last_name}</b></p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <p className="pull-right"><b>Items:</b></p>
                  </div>
                  <div className="col-md-6">
                    <div className="pull-left">
                      <b>{items}</b>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <p className="pull-right"><b>Total Cost:</b></p>
                  </div>
                  <div className="col-md-6">
                    <p className="pull-left"><b>{"₱ "+total}</b></p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-5">
                    <p className="pull-right"><b>Purchase Date:</b></p>
                  </div>
                  <div className="col-md-6">
                  <div className="pull-left"><b>{created_at}</b></div>
                  </div>
                </div>
                <br/>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" onClick={props.update} className="btn btn-primary btn-sm" 
                data-dismiss="modal">Update</button>
              <button type="button" className="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
    </div>
  )
}


PurchaseModal.propTypes = {
  // purchase : React.PropTypes.object,
  // showPurchaseModal : React.PropTypes.bool,
  // handleChange : React.PropTypes.func,
  // closePurchaseModal : React.PropTypes.func,
  // update: React.PropTypes.func,
}

export default PurchaseModal


