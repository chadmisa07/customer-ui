import React from 'react'
import { Chart } from 'react-google-charts'

const SurveyTaskDetails = (props) => {
  return (
    <div className="modal fade" id="surveyTaskDetails">
      <div className="modal-dialog modal-lg">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title" id="myModalLabel">Survey Task Details</h4>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-md-12">
                  <p>&nbsp;&nbsp;<b>Question 1</b></p>
                  <Chart
                    chartType="BarChart"
                    graph_id="BarChart"
                    data={ [['Rating', 'Strongly Agree', 'Agree', 'Neutral','Disagree', 'Strongly Disagree', { role: 'annotation' }],
                            ['Question 1', 10, 24, 20, 32, 18, ''],
                            ['Question 2', 28, 19, 29, 30, 12, ''],
                            ['Question 3', 28, 19, 29, 30, 12, '']]
                          }
                    options={{isStacked: true, legend: 'top'}}
                    width="100%"
                    height="300px"
                    groupWidth="75%"
                  />
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button className="btn btn-sm btn-success" data-dismiss="modal">OK</button>          
              <button className="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
      </div>
    </div>
  )
}

export default SurveyTaskDetails