import React from 'react'

const InfoTable = (props) => {
  return (
    <div className="well span7">
      <div className="row">
       <span className="col-md-2 margin-top-7">Choose Bot:</span>
          <div className="col-md-4">
            <select className="form-control" onChange={props.botChange}>
              <option>Select Bot</option>
              {props.options}
            </select>
          </div>
        </div><br/>
        <div className="row">
          <span className="col-md-12">
            <table className="table table-bordered table-striped table-overflow">
              <tbody>
                <tr>
                  <td><b>Platform:</b>&nbsp; Facebook</td>
                  <td><b>Date Started:</b>&nbsp; 03/29/2017</td>
                </tr>
                <tr>
                  <td><b>Status:</b>&nbsp;</td>
                  <td><a href="#">View Bot Details</a></td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
    </div>
)
}

export default InfoTable


