import React from 'react'
import { Link } from 'react-router'

const Engagement = (props) => {

    return(
        <div className="panel panel-default">
            <div className="panel-heading">
                <i className="fa fa-user-times fa-fw"></i> <b>Engagement</b>
            </div>
            <div className="panel-body">
                {

                     !props.botStatus.order_status ?
                        <div>
                            <center><button type="button" className="btn btn-primary">Attach Task</button></center><br/>
                        </div>
                    :
                    props.botStatus.order_count !== 0 ?
                        <div>   
                            <div className="text-center" style={{color:'#337AB7'}}><h1>Order Task</h1></div><br/>
                            <div className="text-center"><h4><u>Title: Take Out Orders</u></h4></div>
                            <div className="text-center"><h4>Pending Orders: <span style={{color: 'red'}}>5</span></h4></div>
                            <div className="text-center"><h4>Total Orders: 103</h4></div>
                            <div className="text-center"><h4>Yesterday: 15</h4></div>
                            <div className="text-center"><h4>Today's Count</h4></div>
                            <div className="text-center"><h2>20</h2></div>
                            <div><h5><u><Link to="/dashboard/order-details">Details</Link></u></h5></div>
                        </div>
                        :
                        <div>
                            <div className="text-center" style={{color:'#337AB7'}}><h1>Order Task</h1></div><br/>
                            <div className="text-center"><h1>No orders yet</h1></div>
                        </div>
                }
                
            </div>
        </div>
    )
}

export default Engagement