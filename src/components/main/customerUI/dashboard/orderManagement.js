import React from 'react'
import { connect } from 'react-redux'
import Navigation from '../commons/navigation'
import { Link } from 'react-router'
import PurchasesTable from './purchasesTable'
import PurchaseModal from './purchaseModal'
import * as Request from '../requests/requests'
import InfoTable from './infoTable'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    sidebar: state.sidebar,
    bots: state.businessProcesses.bots,
    purchases: state.businessProcesses.purchases,
  }
}

class OrderManagement extends React.Component{
	constructor(props){
		super(props)
		this.state = {}
	}

	componentWillMount() {
	    this.props.dispatch(Request.getBots())
	    this.props.dispatch({type:"SET_CURRENT_URL", payload: this.props.location.pathname})
	}

	botChange = (e) => {
	    this.setState({selectedBot:e.target.value})
	    this.props.dispatch(Request.getPurchases(e.target.value))
	    this.setState({currentBot: e.target.value})
  	}

	componentWillUnmount() {
	    this.props.dispatch({type:'RESET_BUSINESS_PROCESSES'})
	    clearInterval(this.timerID);
	}

	openPurchaseModal(purchase) {
	    this.setState({purchase:purchase})
	    this.setState({currentStatus: purchase.status})
	    this.setState({showPurchaseModal:true})
	}

	closePurchaseModal() {
	    this.setState({showPurchaseModal:false})
	}

	handleChange = (event) => {
	    this.setState({status: event.target.value})
	    this.setState({currentStatus: event.target.value})
	}

	update(){
	    let purchaseId = this.state.purchase.id
	    let status = this.state.status
	    this.props.dispatch(Request.updateStatus(purchaseId, status,this.state.currentBot))
	    this.closePurchaseModal()
	}

	render(){
		let options = this.props.bots.map((item, index) => {
	      return(
	        <option key={index} value={item.id}>{item.name}</option>
	      )
	    })

	    let purchases = this.props.purchases.map((item, index) => {
	      let items = item.items.map((item, index) =>{
	        return (
	          <p key={index}>{item.product.name + " - " + item.quantity}</p>
	        )
	      })
	      return (
	        <tr key={index}>
	          <td><Link href="#showDetailsModal" data-toggle="modal" data-backdrop="static" 
	                onClick={this.openPurchaseModal.bind(this, item)}>{item.id}
	              </Link>
	          </td>
	          <td>{item.user.first_name + " " + item.user.last_name}</td>
	          <td>{items}</td>
	          <td>{item.created_at}</td>
	        </tr>
	      )
	    })

		return (
			<div id="wrapper">

				<Navigation user={this.props.user.first_name}/>
      
		        <div id="page-wrapper">
		            <div className="row">
		                <div className="col-lg-12">
		                    <h1 className="page-header">Order Management</h1>
		                </div>
		            </div>
		            <div className="row">
		            	<div className="col-lg-6">
	                  		<InfoTable botChange={this.botChange.bind(this)} options={options}/>
	                  	</div>
	                </div>
	                <div className="row">
		                <PurchasesTable {...this.props}
		                   purchases={purchases} 
		                   openPurchaseModal={this.openPurchaseModal.bind(this)}/>
	                </div>
	                <div className="row-fluid">
		                <PurchaseModal {...this.state} {...this.props}
		                   closePurchaseModal={this.closePurchaseModal.bind(this)}
		                   handleChange={this.handleChange.bind(this)} 
		                   update={this.update.bind(this)}/>
		            </div>
		        </div>
		    </div>
		)

	}

}

export default connect(mapStateToProps)(OrderManagement)