import React from 'react'

const BotStatus = (props) => {

    return(
        <div className={props.botStatus.running ? "panel panel-green" : "panel border-green"}>
            <div className="panel-heading">
                <div className="row">
                    <div className="col-xs-12 text-left">
                        <div className={!props.botStatus.running? "green font-17":"font-17"}>
                        {!props.botStatus.bot_name?"No Bot Found":props.botStatus.bot_name}</div>
                        <div className="huge">
                            {props.botStatus.running ? "Running": 
                                !props.botStatus.faq_status && !props.botStatus.survey_status && !props.botStatus.order_status?
                                <div className="green">No Task</div>
                            :
                            <center><button data-toggle="modal" data-backdrop="static"
                             data-target="#deployModal" type="button" className="btn btn-success">Deploy</button></center>}
                        </div>
                        <div>&nbsp;</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div className="panel-footer">
                    <span className={!props.botStatus.running? "green pull-left":"pull-left"}>View Details</span>
                    <span className="pull-right"><i className={!props.botStatus.running? "green fa fa-arrow-circle-right":"fa fa-arrow-circle-right"}></i></span>
                    <div className="clearfix"></div>
                </div>
            </a>
        </div>
    )
}

export default BotStatus