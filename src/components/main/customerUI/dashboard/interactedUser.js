import React from 'react'

const InteractedUser = (props) => {

    return(
         <div className={props.botStatus.running ? "panel panel-primary" : "panel border-blue"}>
            <div className="panel-heading">
                <div className="row">
                    <div className="col-xs-3">
                        <i className={!props.botStatus.running? "blue fa fa-user fa-5x":"fa fa-user fa-5x"}></i>
                    </div>
                    <div className="col-xs-9 text-right">
                        <div className={!props.botStatus.running? "blue font-17":"font-17"}>Interacted Users</div>
                        <div className={!props.botStatus.running? "blue huge":"huge"}>{props.users}</div>
                        <div>&nbsp;</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div className="panel-footer">
                    <span className="pull-left">View Details</span>
                    <span className="pull-right"><i className="fa fa-arrow-circle-right"></i></span>
                    <div className="clearfix"></div>
                </div>
            </a>
        </div>
    )
}

export default InteractedUser