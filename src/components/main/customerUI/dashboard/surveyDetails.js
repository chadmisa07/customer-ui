import React from 'react'
import { connect } from 'react-redux'
import Navigation from '../commons/navigation'
import * as Request from '../requests/requests'
import { Chart } from 'react-google-charts'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    sidebar: state.sidebar,
    botStatus: state.botStatus.botStatus,
    surveyDetails: state.survey.surveyDetails
  }
}

class SurveyDetails extends React.Component{
	constructor(props){
		super(props)
		this.state = {}
	}

	componentWillMount(){
		this.props.dispatch(Request.getSurveyDetails(this.props.bot.bots.length?this.props.bot.bots[0].id:""))
	}

	render(){
		let x = 0
		let surveyData = [['Rating']]
		this.props.surveyDetails.questions[0].answers.map( (choices, index) =>{
			x++
			return(
				x!==this.props.surveyDetails.questions[0].answers.length ?
				surveyData[0].push(choices.text)
				:
				surveyData[0].push(choices.text, { role: 'annotation' })
			)
		})

		this.props.surveyDetails.questions.map( (survey , index) => {
		 	surveyData.push([survey.text])
		 	let x = 0
		 	survey.answers.map( (answer, index) =>{
		 		x++
		 		return(
		 			x!==survey.answers.length ?
		 			surveyData[surveyData.length - 1].push(answer.count)
		 			:
		 			surveyData[surveyData.length - 1].push(answer.count,"")
		 		)
		 		
		 	})
		 	return (null)
		 	
		 })

		let choices = this.props.surveyDetails.questions[0].answers.map( (choices, index) =>{
			return(
					<th key={index}>{choices.text} </th>
			)
		})

		let question = this.props.surveyDetails.questions.map( (question, index) =>{
			let answerCount = question.answers.map( (answerCount,index) =>{
				return(
					<td key={index}>{answerCount.count}</td>
				)
			})
			return(
				<tr key={index}>
					<td>{question.text}</td>
					{answerCount}
					<td>{question.total_answers}</td>
				</tr>
			)
		})

		return (
			<div id="wrapper">

				<Navigation user={this.props.user.first_name}/>
      
		        <div id="page-wrapper">
		        	<div className="row">
		        		<h2>Survey Details</h2>
		        		<hr/>
		        	</div>
              		<div className="row">
              			<div className="col-md-9">
              				<table className="table table-bordered table-overflow">
              					<thead>
              						<tr>
              							<th>Question</th>
              							{choices}
              							<th>Total</th>
              						</tr>
              						{question}
              					</thead>
              					<tbody>

              					</tbody>
							</table>
              			</div>
              		</div><br/>
              		<div className="row">
		                <div className="col-md-12">
		                  <Chart
		                    chartType="BarChart"
		                    data={surveyData}
		                    width="80%"
		                    height="300px"
		                    groupWidth="75%"
	                    	options={{isStacked: true, width: '100%', legend: 'top'}}
		                  />
		                </div>
              		</div>
		        </div>
		    </div>
		)

	}

}

export default connect(mapStateToProps)(SurveyDetails)