import React from 'react'

const DeployModal = (props) => {
  return (
   <div className="modal fade" id="deployModal">
      <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title" id="myModalLabel">Deploy Bot</h4>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-12">
                  {
                    props.fb_pages.length?
                    <div>
                      <p><b>Select what Facebook page the attach to bot</b></p>
                      <select className="form-control" onChange={props.handleFBPageSelection} >
                        {props.fbPages}
                      </select>
                    </div>
                    :
                    <div>
                      <p><b>No FB Page found.</b></p>
                      <a href="https://www.facebook.com/v2.8/dialog/oauth?client_id=1038731512904671&
                              redirect_uri=https://solbinc.herokuapp.com/fb/&auth_type=rerequest&scope=email,
                              public_profile,manage_pages,pages_messaging,pages_messaging_subscriptions,publish_pages,publish_actions" 
                              className="btn btn-small btn-primary" >Connect with <i className="fa fa-facebook white"></i>acebook</a>
                    </div>
                  }
                  <div className="clear-10"></div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button className="btn btn-sm btn-primary" data-dismiss="modal" onClick={props.deployBot}>Deploy</button>
              <button className="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>            
            </div>
        </div>
      </div>
    </div>
  )
}

export default DeployModal