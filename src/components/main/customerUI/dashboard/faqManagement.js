import React from 'react'
import { connect } from 'react-redux'
import Navigation from '../commons/navigation'
import * as Request from '../requests/requests'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.user,
    bots: state.businessProcesses.bots,
    purchases: state.businessProcesses.purchases,
    faqs: state.faq.list,
    faqCount: state.faq.faqCount,
    sidebar: state.sidebar,
  }
}

class FAQManagement extends React.Component{
	constructor(props){
		super(props)
		this.state = {}
	}

	componentWillMount() {
	    this.props.dispatch(Request.getFAQ(1))
 	}  

	render(){
		const faqs = this.props.faqs.map((faq, index) => {
	      return (
	        <tr key={index}>
	          <td className="tac">{faq.id}</td>
	          <td className="tac">{faq.name}</td>
	          <td className="tac">{faq.description}</td>
	          <td className="tac">{faq.faq_type}</td>
	          <td className="tac">{faq.clicks}</td>
	          <td className="tac">{faq.users_reached}</td>
	        </tr>
	      )
	    })
		return (
			<div id="wrapper">

				<Navigation user={this.props.user.first_name}/>
      
		        <div id="page-wrapper">
		            <div className="row">
		                <div className="col-lg-12">
		                    <h1 className="page-header">FAQ Management</h1>
		                </div>
		            </div>
		            <div className="row">
		            	<div className="col-lg-6">
	                  		<p>These are the current clicks of your FAQs</p>
	                  	</div>
	                </div>
	                <div className="row">
		                <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
	                      <thead>
	                        <tr>
	                          <th className="tac table-header">ID</th>
	                          <th className="tac table-header">Name</th>
	                          <th className="tac table-header">Description</th>
	                          <th className="tac table-header">Type</th>
	                          <th className="tac table-header">Clicks</th>
	                          <th className="tac table-header">Users Reached</th>
	                        </tr>
	                      </thead>
	                      <tbody> 
	                        {!this.props.faqs.length ?
	                          <tr><td colSpan="5" className="align-center"><i>No data found</i></td></tr> : 
	                          null
	                        }
	                        {faqs}
	                      </tbody>
	                    </table>
	                </div>
		        </div>
		    </div>
		)

	}

}

export default connect(mapStateToProps)(FAQManagement)