import React from 'react'

const AttachTaskModal = (props) => {
  return (
    <div className="modal fade" id="attachTaskModal">
      <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title" id="myModalLabel">Attach Task</h4>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-12">
                  <div className="control-group">
                    {
                      props.surveys.length?
                      <div className="form-group">
                        <label className="">Choose Task</label>
                        <select value={props.taskId} className="form-control" onChange={props.taskHandlerChange}>
                          {props.surveysOptions}
                        </select>
                      </div>
                      :
                      <div>
                        <h5>No Task Found.</h5>
                        <button type="button" className="btn btn-small btn-success"
                          onClick={props.redirectToCreateSurvey}data-dismiss="modal">Create Task</button>
                      </div>
                    }
                  </div>
                  <div className="clear-10"></div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
            {
              props.surveys.length?
                <div>
                  <button type="button" className="btn btn-success btn-small" onClick={props.attachSurveyTask} data-dismiss="modal">Attach</button>
                  <button type="button" className="btn btn-small btn-default" data-dismiss="modal">Cancel</button>
                </div>
              : ""
            }
            </div>
        </div>
      </div>
    </div>
  )
}

export default AttachTaskModal