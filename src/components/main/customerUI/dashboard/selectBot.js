import React from 'react'
import { Form, FormGroup, Col, FormControl } from 'react-bootstrap'

const SelectBot = (props) => {
  return (
    <Form horizontal>
      <FormGroup>
        <Col sm={4}>
          <h5>Choose Bot:</h5>
        </Col>
        <Col sm={7}>
          <select className="form-control"  onChange={props.botChange} >
            <option value="0">Select Task!!</option>
            {props.options}
          </select>
          
        </Col>
      </FormGroup>
    </Form>
  )
}

export default SelectBot