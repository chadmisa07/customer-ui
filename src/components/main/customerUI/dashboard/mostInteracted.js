import React from 'react'

const MostInteracted = (props) => {

    return(
        <div className={props.botStatus.running ? "panel panel-yellow" : "panel border-yellow"}>
            <div className="panel-heading">
                <div className="row">
                    <div className="col-xs-3">
                        <i className={!props.botStatus.running? "yellow fa fa-cog fa-5x":"fa fa-cog fa-5x"}></i>
                    </div>
                    <div className="col-xs-9 text-right">
                        <div className={!props.botStatus.running? "yellow font-17":"font-17"}>Most Interacted</div>
                        <div className={!props.botStatus.running? "yellow huge":"huge"}>
                            {props.task}
                        </div>
                        <div className={props.botStatus.running ? "green":"yellow"}>{!props.percentage?"":props.percentage + "%"}&nbsp;</div>
                    </div>
                </div>
            </div>
            <a href="#">
                <div className="panel-footer">
                    <span className="yellow pull-left">View Details</span>
                    <span className="pull-right"><i className="yellow fa fa-arrow-circle-right"></i></span>
                    <div className="clearfix"></div>
                </div>
            </a>
        </div>
    )
}

export default MostInteracted