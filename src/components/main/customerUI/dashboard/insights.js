import React from 'react'
import { Chart } from 'react-google-charts';
import { Link } from 'react-router'


const Insights = (props) => {

    return(
        <div className="panel panel-default">
            <div className="panel-heading">
                <i className="fa fa-bar-chart-o fa-fw"></i> <b>Insights</b>
                <div className="pull-right">
                    <div className="btn-group">
                        <button type="button" className="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            Actions
                            <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu pull-right" role="menu">
                            <li><a href="#">Action</a>
                            </li>
                            <li><a href="#">Another action</a>
                            </li>
                            <li><a href="#">Something else here</a>
                            </li>
                            <li className="divider"></li>
                            <li><a href="#">Separated link</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="panel-body">
               
                {
                    !props.botStatus.survey_status ?
                        <div>
                            <center>
                                <button type="button" className="btn btn-primary" data-toggle="modal" data-backdrop="static"
                                    data-target="#attachTaskModal">
                                        Attach Task
                                </button>
                            </center><br/>
                        </div>
                    :
                    props.botStatus.survey_count !== 0 ?
                    <div>
                        <div className="text-center" style={{color:'#337AB7'}}><h1>Survey Task</h1></div><br/>
                        <div className="text-center"><h4><u><Link to="/dashboard/survey-details">Title: {props.name}</Link></u></h4></div>
                        <div className="text-center"><h4>Answered: <b>{props.total}</b></h4></div>
                        <div className="text-center"><h4>Overall Rating: </h4></div>
                        <div className="text-center"><h3><b>EXCELLENT</b></h3></div>
                        <div>
                            <center>
                                <Chart
                                  title="asdsa"
                                  chartType="PieChart"
                                  data={props.pieChartData}
                                  options={{'is3D':'true','pieHole':'0.7', title: props.title}}
                                  width="100%"
                                  height="400px"
                                />
                            </center>
                        </div>
                        <div><h5><u><Link to="/dashboard/survey-details">Details</Link></u></h5></div>
                    </div>:
                    <div>
                        <div className="text-center" style={{color:'#337AB7'}}><h1>Survey Task</h1></div><br/>
                        <div className="text-center"><h1>No answers yet</h1></div>
                    </div>
                }
                
            </div>
        </div>
    )
}

export default Insights