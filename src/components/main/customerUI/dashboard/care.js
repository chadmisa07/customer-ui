import React from 'react'
import { Link } from 'react-router'

const Care = (props) => {

    return(
        <div className="panel panel-default">
            <div className="panel-heading">
                <i className="fa fa-plus-square fa-fw"></i> <b>Care</b>
            </div>
            <div className="panel-body">
            {
                !props.botStatus.faq_status ?
                <div>
                    <center><button type="button" className="btn btn-primary">Attach Task</button></center><br/>
                </div>
                :
                props.botStatus.survey_count !== 0 ?
                    <div>
                        <div className="text-center" style={{color:'#337AB7'}}><h1>FAQ Task</h1></div><br/>
                        <div className="text-center"><h4>Most Viewed: Contact Details</h4></div>
                        <div className="text-center"><h4>Pending User Questions: <span style={{color: 'red'}}>10</span></h4></div>
                        <div><h5><u><Link to="/dashboard/faq-details">Details</Link></u></h5></div>
                    </div>
                :
                <div>
                    <div className="text-center" style={{color:'#337AB7'}}><h1>Order Task</h1></div><br/>
                    <div className="text-center"><h1>Order not available</h1></div>
                </div>
            }
                
            </div>
        </div>
    )
}

export default Care