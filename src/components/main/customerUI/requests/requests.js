import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import * as api from '../../../../utils/api'


export function getBot(page) {
  return function(dispatch) {
  dispatch({type: "GET_BOT_REQUEST" })
  axios.get(api.URL+"api/bot/?page="+page)
    .then(function (response) {
      dispatch({type: "GET_BOT_REQUEST_FULFILLED", payload: response.data})
    })
    .catch(function (error) {
      toastr.error('Error', 'Failed to fetch bots')
      dispatch({type: "GET_BOT_REQUEST_REJECTED", payload: error})
    })
  }  
}

export function getJobFlow(id) {
  return function(dispatch) {
    dispatch({type: "GET_JOB_FLOW_REQUEST" })
    axios.get(api.URL+"api/job-flow/?page="+id)
      .then(function (response) {
        dispatch({type: "GET_JOB_FLOW_REQUEST_FULFILLED", payload: response.data})
      })
      .catch(function (error) {
        toastr.error('Error', 'Failed to fetch tasks')
        dispatch({type: "GET_JOB_FLOW_REQUEST_REJECTED", payload: error})
      })
  }  
}

export function getFBPages() {
  return function(dispatch) {
    dispatch({type:'FB_PAGES_FETCH_REQUEST'})
    axios.get(api.URL+"api/fb-page/")
      .then(function (response) {
        dispatch({type:'FB_PAGES_FETCH_REQUEST_FULFILLED', payload: response.data.results})
      })
      .catch(function (error) {
        dispatch({type:'FB_PAGES_FETCH_REQUEST_REJECTED'})
        toastr.error('Error', 'Unable to fetch Facebook Pages')
      })
  }
}

export function deployBot(fb_page, data, access_token) {
  return function(dispatch) {
    dispatch({type:'DEPLOY_BOT_REQUEST'})
    console.log(JSON.stringify(data))
    axios.patch(api.URL+"api/fb-page/"+fb_page.id.toString()+"/", data)
      .then(function (response) {
          axios.post(api.URL+"app/update-thread/", {"fb_page":fb_page.id})
            .then(function (response) {
              delete axios.defaults.headers.common['Authorization']
              axios.defaults.headers.common['Authorization'] = 'Bearer ' + fb_page.page_token
              axios.post("https://graph.facebook.com/v2.8/"+fb_page.page_id+"/subscribed_apps")
              .then(function (response) {
                delete axios.defaults.headers.common['Authorization']
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token
                toastr.success('Success', 'Bot was successfully deployed')
                dispatch({type:'DEPLOY_BOT_REQUEST_FULFILLED'})
                axios.patch(api.URL+"api/bot/"+data.bot+"/",{running: true})
                .then(function(response){
                  dispatch(getBot(1))
                  dispatch(getFBPages())
                  window.open('https://m.me/' + fb_page.page_id)
                  dispatch({type:"SHOW_OVERLAY", payload: false, overlayText: ""})
                  dispatch(getBotStatus(data.bot))
                  dispatch(getLinkedFbPage(data.bot))
                })
                .catch(function (error){
                  dispatch({type: "SUBSCRIBE_REQUEST_REJECTED!" })
                  toastr.error('Error', 'Failed to subcribe apps')
                  dispatch({type:"SHOW_OVERLAY", payload: false, overlayText: ""})
                })
              })
              .catch(function (error) {
                dispatch({type: "SUBSCRIBE_REQUEST_REJECTED!" })
                toastr.error('Error', 'Failed to subcribe apps')
                dispatch({type:"SHOW_OVERLAY", payload: false, overlayText: ""})
              })
            })
            .catch(function (error) {
              dispatch({type:'DEPLOY_BOT_REQUEST_REJECTED'})
              toastr.error('Error', 'Unable to deploy the bot')
              dispatch({type:"SHOW_OVERLAY", payload: false, overlayText: ""})
            })
      })
      .catch(function (error) {
        dispatch({type:'DEPLOY_BOT_REQUEST_REJECTED'})
        toastr.error('Error', 'Unable to deploy the bot')
        dispatch({type:"SHOW_OVERLAY", payload: false, overlayText: ""})
      })
  }
}

export function unlaunchBot(fb_page, data, access_token, botId) {
  return function(dispatch) {
    dispatch({type:'UNLAUCH_BOT_REQUEST'})
    axios.patch(api.URL+"api/fb-page/"+fb_page.id.toString()+"/", data)
      .then(function (response) {
        toastr.success('Success', 'Bot was successfully stopped')
        dispatch(getBot(1))
        dispatch(getFBPages())
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + fb_page.page_token
        axios.delete("https://graph.facebook.com/v2.8/"+fb_page.page_id+"/subscribed_apps")
          .then(function (response) {
            let data = {
              "setting_type":"CALL_TO_ACTIONS",
              "thread_state":"existing_thread"
            }
            let url = "https://graph.facebook.com/v2.6/me/thread_settings?access_token="+fb_page.page_token
            axios.delete(url, {data:data})
              .then(function (response){
                toastr.success('Success', 'Deleted persistent menu')
              })
              .catch(function (error){
                toastr.error('Error', 'Failed to update persistent menu')
              })

            url = "https://graph.facebook.com/v2.6/me/messenger_profile?access_token="+fb_page.page_token
            data = {
              "fields":[
                "greeting"
              ]
            }

            axios.delete(url, {data:data})
              .then(function (response){
                toastr.success('Success', 'Deleted greeting text')
              })
              .catch(function (error){
                toastr.error('Error', 'Failed to delete greeting text')
              })

              axios.patch(api.URL+"api/bot/"+botId+"/",{running: false})
              .then(function (response){
                dispatch(getBotStatus(botId))
                dispatch({type:"GET_LINKED_FB_PAGE", payload: {}})
              })
              .catch(function (error){
                toastr.error('Error', 'Failed to update bot status')
              })
          })
          .catch(function (error) {
            toastr.error('Error', 'Failed to unsubcribe apps')
          })
        delete axios.defaults.headers.common['Authorization']
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token
        dispatch({type:'UNLAUCH_BOT_REQUEST_FULFILLED'})
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to stop the bot')
        dispatch({type:'UNLAUCH_BOT_REQUEST_REJECTED'})
      })
  }
}


export function stopBot(bot, access_token) {
  return function(dispatch) {
    dispatch({type:'STOP_BOT_REQUEST'})
    axios.get(api.URL+"api/fb-page/?bot="+bot.toString())
      .then(function (response) {
       dispatch(unlaunchBot(response.data.results[0], {bot:null}, access_token, bot))
        dispatch({type:'STOP_BOT_REQUEST_FULFILLED'})
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to stop the bot')
        dispatch({type:'STOP_BOT_REQUEST_REJECTED'})
      })
  }
}

export function updateBot(id, task, data) {
  return function(dispatch) {
    dispatch({type: "UPDATE_BOT_REQUEST" })
    axios.put(api.URL+"api/bot/"+id+"/" , data)
      .then(function (response) {
        dispatch({type: "UPDATE_BOT_REQUEST_FULFILLED", payload: response.data})
        toastr.success('Success', 'Bot successfully updated') 
        if(task !== 0){
          let taskData = {bot:id,task:task}
          axios.post(api.URL+"survey/update-bot-task/", taskData)
          .then(function (response){
            toastr.success('Success', 'Successfully updated bot task') 
            dispatch(getBot(1))
          })
          .catch(function (error){
            toastr.error('Error', 'Failed to update bot task') 
          })
        }else{
          dispatch(getBot(1))
        }
      })
      .catch(function (error) {
        toastr.error('Error', 'Failed to update bot')
        dispatch({type: "UPDATE_BOT_REQUEST_REJECTED", payload: error})
      })
  }  
}

export function getBots() {
  return function(dispatch) {
    dispatch({type: "FETCH_BOTS_REQUEST"})
    axios.get(api.URL+"api/bot/")
      .then(function(response) {
        dispatch({type:'FETCHED_BOTS_BUSINESS_PROCESSES', payload: response.data.results})
        dispatch({type: "FETCH_BOTS_REQUEST_FULFILLED"})
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to fetch bots')
        dispatch({type: "FETCH_BOTS_REQUEST_REJECTED"})
      })
  }
}


export function getPurchases(id) {
  return function(dispatch) {
    dispatch({type: "GET_PURCHASES_REQUEST"})
    axios.get(api.URL+"api/user-purchase/?bot="+id)
      .then(function(response) {
        dispatch({type:'FETCHED_PURCHASES_BUSINESS_PROCESSES', payload: response.data.results})
        dispatch({type: "GET_PURCHASES_REQUES_FULFILLED"})
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to fetch purchases')
        dispatch({type: "GET_PURCHASES_REQUEST_REJECTED"})
      })
  }
}

export function updateStatus(purchaseId, data, currentBot) {
  return function(dispatch) {
    dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST" })
    axios.patch(api.URL+"api/user-purchase/"+purchaseId+"/", {status:data})
      .then(function (response) {
        toastr.success('Success', 'Purchase status successfully updated.')
        dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST_FULFILLED", payload: response.data})
        dispatch(getPurchases(currentBot))
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to update purchase status')
        dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST_REJECTED", payload: error})
      })
  }  
}

export function getBotStatus(id){
  return function(dispatch){
    dispatch({type:"GET_BOT_STATUS"})
    axios.post(api.URL+"app/bot-status/", {"bot":id})
    .then(function (response){
      dispatch({type: "GET_BOT_STATUS_FULFILLED", payload: response.data})
      dispatch(getBots())
    })
    .catch(function (error){
      dispatch({type: "GET_BOT_STATUS_REJECTED", payload: error})
    })
  }
}

export function getSurveyDetails(id){
  return function(dispatch){
    dispatch({type:"GET_SURVEY_DETAILS_REQUEST"})
    axios.post(api.URL+"app/get-insights/", {"bot":id})
    .then(function (response){
      dispatch({type: "GET_SURVEY_DETAILS", payload: response.data.result})
      dispatch({type: "GET_SURVEY_DETAILS_REQUEST_FULFILLED"})
    })
    .catch(function (error){
      dispatch({type: "GET_SURVEY_DETAILS_REQUEST_REJECTED", payload: error})
    })
  }
}

export function getFAQ(page) {
  return function(dispatch) {
    dispatch({type: "GET_FAQ_REQUEST"})
    axios.get(api.URL+"api/faq/?page="+page)
      .then(function (response) {
        let results = response.data.results
        for (let result of results){
          result['forUpdate'] = false
        }
        dispatch({type:"FAQ_LIST_FETCHED", payload:results, count: response.data.count})
        dispatch({type: "GET_FAQ_REQUEST_FULFILLED"})
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to fetch FAQ')
        dispatch({type: "GET_FAQ_REQUEST_REJECTED"})
      })
  }
}

export function getSurvey(page){
  return function(dispatch) {
    dispatch({type: "GET_SURVEY_REQUEST"})
    axios.get(api.URL+"api/survey/?page="+page)
      .then(function (response) {
        dispatch({type: "GET_SURVEY_REQUEST_FULFILLED", payload: response.data.results, count: response.data.count})
      })
      .catch(function (error) {
        dispatch({type: "GET_SURVEY_REQUEST_REJECTED"})
      })
  }
}

export function createAnswers(answers, response) {
  for (let answer of answers){
    answer["question"] = response.data.id
  }
  axios.post(api.URL+"api/answer/", answers)
    .then(function (response) {
      toastr.success('Success', 'Successfully created the question')
    })
    .catch(function (error) {
      toastr.error('Error', 'Unable to create answers')
    })
}

export function getTasks(){
  return function(dispatch) {
    axios.get(api.URL+"api/job-flow/")
      .then(function (response) {
        dispatch({type: "FETCHED_TASKS", payload: response.data.results})
      })
      .catch(function (error) {
      })
  }
}

export function createSurvey(data) {
  return function(dispatch) {
    axios.post(api.URL+"api/survey/", data)
      .then(function (response) {
        dispatch({type: "SURVEY_CREATED", payload: response.data})
        toastr.success('Success', 'Survey was created')
        dispatch(getSurvey(1))
        dispatch(getTasks())
      })
      .catch(function (error) {
        toastr.error('Error', 'Please enter valid inputs')
      })
  }  
}

export function createQuestion(questions) {
  return function(dispatch) {
    for (let question of questions){
      let answers = question.answers
      axios.post(api.URL+"api/question/", question)
        .then(createAnswers.bind(null, answers))
        .catch(function (error) {
          toastr.error('Error', 'Unable to create a question')
        })
    }
  }  
}

export function updateSurveyType(survey, type) {
  return function(dispatch) {
    axios.patch(api.URL+"api/survey/"+survey+"/", {"survey_type": type})
      .then(function (response) {
        toastr.success('Success', 'Successfully updated the survey')
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to update the survey type')
      })
  }
}

export function updateSurvey(survey) {
  return function(dispatch) {
    dispatch({type: "UPDATE_SURVEY_REQUEST"})
    axios.patch(api.URL+"api/survey/"+survey.id+"/", survey)
      .then(function (response) {
        toastr.success('Success', 'Successfully updated the survey')
        dispatch({type: "UPDATE_SURVEY_REQUEST_FULFILLED"})
        dispatch(getSurvey(1))
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to update the survey type')
        dispatch({type: "UPDATE_SURVEY_REQUEST_REJECTED"})
      })
  }
}

export function createJobFlow(data, botId) {
  return function(dispatch) {
    axios.post(api.URL+"api/job-flow/", data)
      .then(function (response) {
        toastr.success('Success', 'Successfully created task')
        dispatch({type: "TASK_CREATED", payload: response.data})
        dispatch(getSurvey(1))
        dispatch(getTasks())
        dispatch({type:"CLEAR_TASK"})
        if(botId){
          let taskData = {bot:botId,task:response.data.id}
          axios.post(api.URL+"survey/update-bot-task/", taskData)
          .then(function (response){
            toastr.success('Success', 'Successfully linked task to bot') 
            dispatch({type:"CLEAR_BOT"})
            dispatch({type:"CLEAR_TASK"})
          })
          .catch(function (error){
            toastr.error('Error', 'Failed to update bot task ' + error) 
          })
        }
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to create task')
      })
  }
}

export function getSurveys(){
  return function(dispatch) {
    dispatch({type: "GET_SURVEY_REQUEST"})
    axios.get(api.URL+"api/survey/")
      .then(function (response) {
        dispatch({type: "GET_SURVEY_REQUEST_FULFILLED", payload: response.data.results})
      })
      .catch(function (error) {
        dispatch({type: "GET_SURVEY_REQUEST_REJECTED"})
      })
  }
}

export function attachSurveysToTask(surveys, task){
  return function(dispatch) {
    for (let survey of surveys){
      try{
        if (survey.forUpdate){
          // alert(survey.name)
          axios.patch(api.URL+"api/survey/"+String(survey.id)+"/", {"task_flow":task})
            .then(function (response) {
              toastr.success('Success','Successfully attached survey to task')
              // dispatch(getSurveys())
              dispatch(getSurvey(1))
            })
            .catch(function (error) {
              toastr.error('Error','Unable to attach survey to task')
            })
        } else {
          if (String(survey.task_flow)===String(task)){
            axios.patch(api.URL+"api/survey/"+String(survey.id)+"/", {"task_flow":null})
              .then(function (response) {
                toastr.success('Success','Successfully removed survey to task')
                // dispatch(getSurveys())
                dispatch(getSurvey(1))
              })
              .catch(function (error) {
                toastr.error('Error','Unable to remove survey to task')
              })
          }
        }
      } catch(e) {

        // Ignore error

        // axios.patch("https://cbot-api.herokuapp.com/api/survey/"+String(survey.id)+"/", {"task_flow":null})
        //   .then(function (response) {
        //     toastr.success('Success','Successfully attached survey to task')
        //   })
        //   .catch(function (error) {
        //     toastr.error('Error','Unable to attach survey to task')
        //   })
      }
    }
  }
}

export function getUserSurvey(){
  return function(dispatch) {
    dispatch({type: "GET_USER_SURVEY_REQUEST"})
    axios.get(api.URL+"api/user-survey/")
      .then(function (response) {
        dispatch({type: "GET_USER_SURVEY_REQUEST_FULFILLED", payload: response.data.results})
        console.log(JSON.stringify(response.data.results))
      })
      .catch(function (error) {
        dispatch({type: "GET_USER_SURVEY_REQUEST_REJECTED"})
      })
  }
}

export function attachTaskToSurvey(surveyId,taskId, botId){
  return function(dispatch){
    dispatch({type: "ATTACH_TASK_REQUEST"})
    axios.patch(api.URL+"api/user-survey/"+surveyId+"/", {task_flow: taskId})
    .then(function (response){
      dispatch({type: "ATTACH_TASK_REQUEST_FULFILLED"})
      dispatch(getFBPages())
      dispatch(getUserSurvey())
      dispatch(getBotStatus(botId))
      dispatch(getSurveyDetails(botId))
    })
    .catch(function (error){
      dispatch({type: "ATTACH_TASK_REQUEST_REJECTED"})
    })
  }
}

export function getLinkedFbPage(botId){
  return function(dispatch){
    dispatch({type: "ATTACH_TASK_REQUEST"})
    axios.get("https://www.ntuitiv.io/api/fb-page/?bot="+botId)
    .then(function (response){
      dispatch({type:"GET_LINKED_FB_PAGE", payload: response.data.results[0]})
      dispatch({type: "ATTACH_TASK_REQUEST_FULFILLED"})
    })
    .catch(function (error){
      dispatch({type: "ATTACH_TASK_REQUEST_REJECTED"})
    })
  }
}

