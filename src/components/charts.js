import React from 'react';
import fusioncharts from 'fusioncharts';
import charts from 'fusioncharts/fusioncharts.charts';
import { Chart } from 'react-google-charts';


class Charts extends React.Component{

  render() {

    charts(fusioncharts)

    // var myDataSource = {
    //     chart: {
    //         caption: "How would you rate our service?",
    //         subCaption: "",
    //         numberPrefix: "",
    //         theme: "ocean"
    //     },
    //     data: [{
    //         label: "Excellent",
    //         value: "1"
    //     }, {
    //         label: "Satisfactory",
    //         value: "0"
    //     }, {
    //         label: "Needs Improvement",
    //         value: "3"
    //     }]
    // };

    // var revenueChartConfigs = {
    //     id: "revenue-chart",
    //     type: "column2d",
    //     width: "30%",
    //     height: 300,
    //     dataFormat: "JSON",
    //     dataSource: myDataSource
    // };

    return (
      <div>
        <Chart
          chartType="PieChart"
          data={[["Task","Hours per Day"],["Work",11],["Eat",2],["Commute",2],["Watch TV",2],["Sleep",7], ["Chad", 30]]}
          options={{}}
          width="60%"
          height="400px"
        />
      </div>
    )
  }
}

export default Charts


