import React from 'react'
import { connect } from 'react-redux'
import '../includes/css/toastr.css'
import '../includes/css/style.css'

class App extends React.Component{
  render() {
    return (
      this.props.children
    )
  }
}

const mapStateToProps = (state) => {
  return {
    task: state.createSurvey.task,
    questions: state.createSurvey.questions,
    survey: state.createSurvey.survey,
    user: state.user.id,
    surveys: state.createSurvey.surveys,
    tasks: state.createSurvey.tasks
  }
}

export default connect(mapStateToProps)(App)