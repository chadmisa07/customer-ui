import React, { Component } from 'react'
import { Link } from 'react-router'

class BotFlowSideBar extends Component{
  render(){
    return (
      <div className="flow-left-side-bar">
        <ul className="sidebar">
          <li className="sidebar">
            <Link className="sidebar-header">Bot Flow Editor</Link>
          </li>
          <li className="sidebar">
            <Link className="sidebar-text text-center">Here you can edit your bots</Link>
          </li>
          <li className="sidebar">
            <Link className="sidebar">Order Bot</Link>
          </li>
          <li className="sidebar">
            <Link to="/bot/order_menu"
              className={this.props.selected==='editMenu'? "sidebar active":"sidebar"}>1. Menu</Link>
          </li>
          <li className="sidebar">
            <Link to="/bot/order_count"
              className={this.props.selected==='orderCount'? "sidebar active":"sidebar"}>2. Order Count</Link>
          </li>
          <li className="sidebar">
            <Link to="/bot/order_cart"
              className={this.props.selected==='editCart'? "sidebar active":"sidebar"}>3. Cart</Link>
          </li>
          <li className="sidebar">
            <Link to="/bot/order_delivery"
              className={this.props.selected==='editDelivery'? "sidebar active":"sidebar"}>4. Delivery / Pickup</Link>
          </li>
          <li className="sidebar">
            <Link to="/bot/checkout"
              className={this.props.selected==='editCheckout'? "sidebar active":"sidebar"}>5. Checkout</Link>
          </li>
        </ul>
      </div>
    )
  }
}

export default BotFlowSideBar