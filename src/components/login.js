import React from 'react'
import { connect } from 'react-redux'
import { toastr } from 'react-redux-toastr'
import { login } from '../actions/loginActions'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  }
}

class Login extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      usernameInput: "",
      passwordInput: "",
      erorr: ""
    }
  }


  login() {
    let username = this.state.usernameInput
    let password = this.state.passwordInput

    if(!username.replace(/\s/g, '').length){
      toastr.error('Error', 'Please enter a username')
    }else if(!password.replace(/\s/g, '').length){
      toastr.error('Error', 'Please enter a password')
    }else{
      this.props.dispatch(login(username, password))
    }
  }

  handleKeyPress = (event) => {
    if(event.key === 'Enter'){
      this.login()
    }
  }

  handleUsernameChange = (event) => {
    this.setState({ usernameInput: event.target.value })
  }

  handlePasswordChange = (event) => {
    this.setState({ passwordInput: event.target.value })
  }

  clearInputs(){
    this.setState({ passwordInput: "" })
    this.setState({ usernameInput: "" })
  }

  render() {
		return (
      <div className="container">
        <div className="row">
            <div className="col-md-4 col-md-offset-4">
                <div className="login-panel panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Please Sign In</h3>
                    </div>
                    <div className="panel-body">
                        {
                          this.props.auth.error != null? 
                            <div className="alert alert-danger alert-login tac">{this.props.auth.error}</div>
                            :
                            null
                        }
                        <form role="form">
                            <fieldset>
                                <div className="form-group">
                                    <input className="form-control" 
                                      onChange={this.handleUsernameChange.bind(this)} 
                                      placeholder="Username" name="username" type="email" value={this.state.usernameInput}/>
                                </div>
                                <div className="form-group">
                                    <input className="form-control" 
                                      onChange={this.handlePasswordChange.bind(this)} 
                                      placeholder="Password" name="password" type="password" 
                                      value={this.state.passwordInput}
                                      onKeyPress={this.handleKeyPress.bind(this)}/>
                                </div>
                                <div className="form-group pull-right">
                                  <button type="button" onClick={this.login.bind(this)} 
                                    className="btn btn-sm btn-success"
                                    disabled={this.props.auth.fetching}>
                                      {this.props.auth.fetching ? 'Signing in...' : 'Sign in'}
                                  </button>&nbsp;
                                  <button type="button" className="btn btn-sm btn-default" onClick={this.clearInputs.bind(this)}>Clear</button>
                                </div>
                                
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
		)
  }
}

export default connect(mapStateToProps)(Login)
