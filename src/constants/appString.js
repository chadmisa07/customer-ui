

export function getString(text, language){
	
	if(language === "EN"){
		return getEnglishString(text)
	}else if(language === "JP"){
		return getJapaneseString(text)
	}
}

export function getEnglishString(text){
	let message = null
	switch(text){
		case "bot_creation":{
			message = "Bot Creation"
			break
		}
		case "manage_bot_task":{
			message = "Manage Bot Task"
			break
		}
		case "bot_administration":{
			message = "Bot Administration"
			break
		}
		case "business_process":{
			message = "Business Process"
			break
		}
		case "post_to_fb":{
			message = "Post to Facebook"
			break
		}
		case "create_new_bot":{
			message = "Create New Bot"
			break
		}
		case "this_is_where_you_will":{
			message = "This is where you will"
			break
		}
		case "create_a_new_bot":{
			message = "create a new bot"
			break
		}
		case "the_steps_involved_are":{
			message = "The Steps involved are"
			break
		}
		case "name_and_describe_your_bot":{
			message = "Name and describe your bot"
			break
		}
		case "select_what_bot_does":{
			message = "Select what the Bot does to give it its \"task\""
			break
		}
		case "create_bot_additional_info_example":{
			message = "ie: Take Orders, Do Survey, etc."
			break
		}
		case "edit_the_menu_texts":{
			message = "Edit the menu texts, flow and responses to fit to your needs"
			break
		}
		case "additional_info_test_and_launch":{
			message = "Then you're ready to test and launch it!"
			break
		}
		case "lets_get_started":{
			message = "Let's get started!"
			break
		}
		case "hide_additional_info":{
			message = "Hide Additional Info"
			break
		}
		case "show_additional_info":{
			message = "Show Additional Info"
			break
		}
		case "create_bot":{
			message = "Create Bot"
			break
		}
		case "platform": {
			message = "Platform"
			break
		}
		case "enter_name":{
			message = "Enter Name"
			break
		}
		case "enter_description":{
			message = "Enter Description"
			break
		}
		case "edit_delete_bot_you_created":{
			message = "edit or delete bots that you created, as well as change its tasks"
			break
		}
		case "select_which_bot_to_edit":{
			message = "Select which bot to edit"
			break
		}
		case "change_description_or_linked_tasks":{
			message = "You can change its descriptions or change the linked tasks"
			break
		}
		case "save_as_new_and_relaunch":{
			message = "Save as new bot and relaunch it"
			break
		}
		case "edit_bots":{
			message = "Edit Bots"
			break
		}
		case "select_bot_to_edit_by_pressing":{
			message = "Select which bot to edit by pressing the Edit button beside it"
			break
		}
		case "edit":{
			message = "Edit"
			break
		}
		case "delete":{
			message = "delete"
			break
		}
		case "hi":{
			message = "Hi"
			break
		}
		case "connect_with_facebook":{
			message = "Connect with Facebook"
			break
		}
		case "dashboard":{
			message = "Dashboard"
			break
		}
		case "change_language":{
			message = "Change Language"
			break
		}
		case "change_password":{
			message = "Change Password"
			break
		}
		case "logout":{
			message = "Logout"
			break
		}
		case "bot_id":{
			message = "Bot ID"
			break
		}
		case "bot_name":{
			message = "Bot Name"
			break
		}
		case "bot_description":{
			message = "Bot Description"
			break
		}
		case "jobs_defined":{
			message = "Jobs Defined"
			break
		}
		case "actions":{
			message = "Actions"
			break
		}
		case "choose_task":{
			message = "Choose Task"
			break
		}
		case "update":{
			message = "Update"
			break
		}
		case "close":{
			message = "Close"
			break
		}
		case "update_bot":{
			message = "Update Bot"
			break
		}
		case "delete_bot":{
			message = "Delete Bot"
			break
		}
		case "you_are_about_to_delete":{
			message = "You are about to delete"
			break
		}
		case "are_you_sure_you_want_to_delete":{
			message = "Are you sure you want to delete this bot"
			break
		}
		case "cancel":{
			message = "Cancel"
			break
		}

		default:{
			message = "Error"
		}
	}
	return message
}

export function getJapaneseString(text){
	let message = null
	switch(text){

		case "bot_creation":{
			message = "ボットの作成"
			break
		}
		case "manage_bot_task":{
			message = "ボットタスクの管理"
			break
		}
		case "bot_administration":{
			message = "ボット管理"
			break
		}
		case "business_process":{
			message = "ビジネスプロセス"
			break
		}
		case "post_to_fb":{
			message = "に投稿フェイスブック"
			break
		}
		case "create_new_bot":{
			message = "新しいボットを作成する"
			break
		}
		case "this_is_where_you_will":{
			message = "これがあなたの"
			break
		}
		case "create_a_new_bot":{
			message = "新しいボットを作成する"
			break
		}
		case "the_steps_involved_are":{
			message = "関係するステップは"
			break
		}
		case "name_and_describe_your_bot":{
			message = "あなたのボットに名前をつけて説明する"
			break
		}
		case "select_what_bot_does":{
			message = "ボットがその「タスク」を与えるために行うことを選択します。"
			break
		}
		case "create_bot_additional_info_example":{
			message = "すなわち：受注、調査など。"
			break
		}
		case "edit_the_menu_texts":{
			message = "ニーズに合わせてメニューのテキスト、フロー、レスポンスを編集する"
			break
		}
		case "additional_info_test_and_launch":{
			message = "次にテストして起動する準備が整いました"
			break
		}
		case "lets_get_started":{
			message = "始めましょう！"
			break
		}
		case "hide_additional_info":{
			message = "追加情報を隠す"
			break
		}
		case "show_additional_info":{
			message = "追加情報を表示"
			break
		}
		case "create_bot":{
			message = "ボットの作成"
			break
		}
		case "platform": {
			message = "プラットフォーム"
			break
		}
		case "enter_name":{
			message = "名前を入力"
			break
		}
		case "enter_description":{
			message = "説明を入力"
			break
		}
		case "edit_delete_bot_you_created":{
			message = "作成したボットの編集や削除、タスクの変更"
			break
		}
		case "select_which_bot_to_edit":{
			message = "編集するボットを選択"
			break
		}
		case "change_description_or_linked_tasks":{
			message = "その説明を変更したり、リンクされたタスクを変更することができます"
			break
		}
		case "save_as_new_and_relaunch":{
			message = "新しいボットとして保存して再起動する"
			break
		}
		case "edit_bots":{
			message = "ボットを編集する"
			break
		}
		case "select_bot_to_edit_by_pressing":{
			message = "その横にある「編集」ボタンを押して、編集するボットを選択します"
			break
		}
		case "edit":{
			message = "編集"
			break
		}
		case "delete":{
			message = "削除"
			break
		}
		case "hi":{
			message = "こんにちは"
			break
		}
		case "connect_with_facebook":{
			message = "に接続するフェイスブック"
			break
		}
		case "dashboard":{
			message = "ダッシュボード"
			break
		}
		case "change_language":{
			message = "言語を変えてください"
			break
		}
		case "change_password":{
			message = "パスワードを変更する"
			break
		}
		case "logout":{
			message = "ログアウト"
			break
		}
		case "bot_id":{
			message = "ボットイド"
			break
		}
		case "bot_name":{
			message = "ボット名"
			break
		}
		case "bot_description":{
			message = "ボットの説明"
			break
		}
		case "jobs_defined":{
			message = "定義されたジョブ"
			break
		}
		case "actions":{
			message = "行動"
			break
		}
		case "choose_task":{
			message = "タスクを選択"
			break
		}
		case "update":{
			message = "更新"
			break
		}
		case "close":{
			message = "閉じる"
			break
		}
		case "update_bot":{
			message = "ボットを更新する"
			break
		}
		case "delete_bot":{
			message = "ボットの削除"
			break
		}
		case "you_are_about_to_delete":{
			message = "あなたは削除しようとしています"
			break
		}
		case "are_you_sure_you_want_to_delete":{
			message = "このボットを削除してもよろしいですか？"
			break
		}
		case "cancel":{
			message = "キャンセル"
			break
		}

		default:{
			message = "Error"
		}

	}
	return message
}