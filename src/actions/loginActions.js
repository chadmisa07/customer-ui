import { getToken, clearToken } from '../requests/auth'

export function login(username, password){
	return getToken(username, password)
}

export function logout(username, token){
	return clearToken(username, token)
}
