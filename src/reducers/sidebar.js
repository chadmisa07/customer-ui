const default_state = {
	show: true,
}

export default function reducer(state=default_state, action){

	switch(action.type) {
		case "SHOW_SIDEBAR": 
			return {...state, show: action.payload }
		default: return state
	}
	
}