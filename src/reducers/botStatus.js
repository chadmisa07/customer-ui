const default_state = {
  botStatus: {},
  error: ""
}

export default function reducer(state=default_state, action) {
  switch(action.type) {
   
    case "GET_BOT_STATUS_FULFILLED": {
      return {...state, botStatus: action.payload, error: ""}
    }
    case "GET_BOT_STATUS_REJECTED": {
      return {...state, botStatus: {}, error: "ERROR GETTING BOT STATUS"}
    }
    default: {
      return {...state}
    }
  }
}