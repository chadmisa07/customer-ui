const default_state = {
  task: {},
  data: {},
  tasks: [],
  menu_interface: [],
  count: 0,
  platform: null,
  name: null,
  description: null,
  creating: false,
  success: false,
  failed: false,
  error: null,
  fetching: false,
  fetched: false,
  menuInterfaceStatus: false,
  buttonResponseStatus: false,
  textResponseStatus: false,
  quickReplyStatus: false,
  locationInterfaceStatus : false,
  greetingTextStatus : false,
  persistentMenuStatus: false
}
export default function reducer(state=default_state, action){
  switch(action.type) {
    case "GET_JOB_FLOW_REQUEST":{
       return {...state, fetching: true, fetched: false, success: false}
    }
    case "GET_JOB_FLOW_REQUEST_FULFILLED":{
      return {...state, success: true, fetching: false, fetched: true, tasks: action.payload.results, count: action.payload.count}
    }
    case "GET_JOB_FLOW_REQUEST_REJECTED":{
        return {...state, success: false, fetching: false, failed: true, error: action.payload}
    }

    case "GET_TASK_REQUEST":{
      return {...state, fetching: true, fetched: false, success: false, failed: false, task: {}, data: {}}
    }
    case "GET_TASK_REQUEST_FULFILLED":{
      return {...state, success: true, fetched: true, fetching: false, task: action.payload, data: action.data, name: action.data.name, menu_interface: action.data.menu_interface}
    }
    case "GET_TASK_REQUEST_REJECTED":{
      return {...state, success: false, fetching: false, failed: true, error: action.payload, task: {}, data: {}}
    }

    case "SET_MENU_INTERFACE_STATUS":{
      return {...state, menuInterfaceStatus: action.payload}
    }
    case "SET_TEXT_RESPONSE_STATUS":{
      return {...state, textResponseStatus: action.payload}
    }
    case "SET_BUTTON_RESPONSE_STATUS":{
      return {...state, buttonResponseStatus: action.payload}
    }
    case "SET_QUICK_REPLY_STATUS":{
      return {...state, quickReplyStatus: action.payload}
    }
    case "SET_LOCATION_INTERFACE_STATUS":{
      return {...state, locationInterfaceStatus: action.payload}
    }
    case "SET_GREETING_TEXT_STATUS": {
      return {...state, greetingTextStatus: action.payload}
    }
    case "SET_PERSISTENT_MENU_STATUS" :{
      return {...state, persistentMenuStatus: action.payload}
    }
    default: {
      return {...state}
    }
  }
  
}