const default_state = {
  bots: [],
  bot: {},
  count: 0,
  platform: null,
  name: null,
  description: null,
  creating: false,
  success: false,
  failed: false,
  error: null,
  fetching: false,
  fetched: false,
  showOverlay: false,
  overlayText: "",
  currentUrl: null,
  language: "EN",
  languages:[
    {
      description: "English",
      value: "EN"
    },{
      description: "Japanese",
      value: "JP"
    }
  ]
}

export default function reducer(state=default_state, action){
  switch(action.type) {
    case "CREATE_BOT_REQUEST": {
      return {...state, creating: true}
    }
    case "CREATE_BOT_REQUEST_FULFILLED":{
      return {...state, creating: false, success: true, bot: action.payload}
    }
    case "CREATE_BOT_REQUEST_REJECTED":{
      return {...state, creating: false, failed: true, error: action.payload}
    }
    case "RESET_BOT": {
      return state
    }
    case "GET_BOT_REQUEST":{
       return {...state, fetching: true}
    }
    case "GET_BOT_REQUEST_FULFILLED":{
      return {...state, fetching: false, fetched: true, bots: action.payload.results, count: action.payload.count}
    }
    case "GET_BOT_REQUEST_REJECTED":{
        return {...state, fetching: false, failed: true, error: action.payload}
    }
    case "SHOW_OVERLAY":{
      return{...state, showOverlay: action.payload, overlayText: action.overlayText}
    }
    case "CLEAR_BOT":{
      return{...state, bot: {}}
    }
    case "SET_CURRENT_URL":{
      return{...state, currentUrl: action.payload}
    }
    case "SET_LANGUAGE":{
      return{...state, language: action.payload}
    }
    default: {
      return state
    }
  }
  
}