const default_state = {
  fbAccessToken: {},
  appAccessToken: {},
  pages: [],
  success: false,
  failed: false,
  error: null,
  fetching: false,
  fetched: false,
  postAccessToken: {},
  linkedPage: {}
}

export default function reducer(state=default_state, action){
  switch(action.type) {
    case "GET_FB_ACCESS_TOKEN_REQUEST_REQUEST": {
      return {...state, creating: true}
    }
    case "GET_FB_ACCESS_TOKEN_REQUEST_FULFILLED":{
      return {...state, creating: false, success: true, fbAccessToken: action.payload}
    }
    case "GET_FB_ACCESS_TOKEN_REQUEST_REJECTED":{
      return {...state, creating: false, failed: true, error: action.payload}
    }
    case "GET_FB_PAGE_ACCESS_TOKEN_REQUEST": {
      return {...state, creating: true, sucess: false}
    }
    case "GET_FB_APP_ACCESS_TOKEN_REQUEST_FULFILLED":{
      return {...state, creating: false, success: true, appAccessToken: action.payload}
    }
    case "GET_FB_APP_ACCESS_TOKEN_REQUEST_REJECTED":{
      return {...state, creating: false, failed: true, error: action.payload}
    }
    case "GET_FB_ACCESS_TOKEN_DEBUG_REQUEST": {
      return {...state, creating: true, sucess: false}
    }
    case "GET_FB_ACCESS_TOKEN_DEBUG_REQUEST_FULFILLED":{
      return {...state, creating: false, success: true, pages: action.payload}
    }
    case "GET_FB_ACCESS_TOKEN_DEBUG_REQUEST_REJECTED":{
      return {...state, creating: false, failed: true, error: action.payload}
    }
    case "GET_FB_PAGES":{
      return {...state, pages: action.payload}
    }
    case "GET_LINKED_FB_PAGE":{
      return {...state, linkedPage: action.payload}
    }
    case "POST_ACCESS_TOKEN_RECEIVED":{
       return {...state, postAccessToken: action.payload}
    }
    default: {
      return state
    }
  }
  
}