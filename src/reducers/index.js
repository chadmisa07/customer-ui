import { combineReducers } from 'redux'

import app from './app'
import auth from './auth'
import bot from './bot'
import user from './user'
import textResponse from './textResponse'
import menuInterface from './menuInterface'
import delivery from './delivery'
import quickReply from './quickReply'
import buttonResponse from './buttonResponse'
import persistentMenu from './persistentMenu'
import greetingText from './greetingText'
import taskEditor from './taskEditor'
import botAdministration from './botAdministration'
import businessProcesses from './businessProcesses'
import socialMedia from './socialMedia'
import createSurvey from './createSurvey'
import {reducer as toastrReducer} from 'react-redux-toastr'
import task from './task'
import { loadingBarReducer } from 'react-redux-loading-bar'
import facebook from './facebook'
import survey from './survey'
import faq from './faq'
import sidebar from './sidebar'
import botStatus from './botStatus'

export default combineReducers({
  auth,
  bot,
  user,
  toastr: toastrReducer,
  textResponse,
  menuInterface,
  delivery,
  quickReply,
  buttonResponse,
  persistentMenu,
  greetingText,
  app,
  taskEditor,
  task,
  botAdministration,
  businessProcesses,
  loadingBar: loadingBarReducer,
  socialMedia,
  facebook,
  createSurvey,
  survey,
  faq,
  sidebar,
  botStatus
})