export default function reducer(state, action) {
  switch(action.type) {
    case "QUICK_REPLY_CREATED": {
      return {
        ...state,
        arr: [...state.arr, action.payload]
      }
    }
    case "RESET_QUICK_REPLY": {
      return (
        {arr:[]}
      )
    }
    default: {
        return {...state}
    }
  }
}