export default function reducer(state={}, action) {
  switch(action.type) {
    case "PERSISTENT_MENU_CREATED": return action.payload
    default: return state
  }
}