const default_state = {
  list: [],
  faqCount: 0
}

export default function reducer(state=default_state, action) {
  switch(action.type) {
    case "FAQ_LIST_FETCHED": {
       return {...state, list: action.payload, faqCount: action.count}
    }
    default: {
      return state
    }
  }
}