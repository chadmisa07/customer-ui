const default_state = {
  result: [],
  survey: [],
  surveyCount: 0,
  success: false,
  failed: false,
  error: null,
  fetching: false,
  fetched: false,
  forEdit: {},
  edit: false,
  surveyDetails: {},
  surveys:[],
}

export default function reducer(state=default_state, action) {
  switch(action.type) {
    
    case "GET_SURVEY_DETAILS":{
      return {...state, surveyDetails: action.payload}
    }

    case "GET_SURVEY_REQUEST": {
       return {...state, fetching: true}
    }
    case "GET_SURVEY_REQUEST_FULFILLED": {
      return {...state, fetching: false, fetched: true, success: true, survey: action.payload, surveyCount: action.count}
    }
    case "GET_SURVEYREQUEST_REJECTED": {
      return {...state, fetching: false, error: "Failed to get survey" }
    }

    case "GET_USER_SURVEY_REQUEST_FULFILLED":{
      return {...state, surveys: action.payload}
    }

    case "GET_SURVEY_RESULT_REQUEST": {
       return {...state, fetching: true}
    }
    case "GET_SURVEY_RESULT_REQUEST_FULFILLED": {
      return {...state, fetching: false, fetched: true, success: true, result: action.payload}
    }
    case "GET_SURVEY_RESULT_REQUEST_REJECTED": {
      return {...state, fetching: false, error: "Failed to get results" }
    }

    case "SET_SURVEY_FOR_EDIT":{
      return {...state, forEdit: action.payload } 
    }

    case "EDIT_SURVEY":{
      return {...state, edit: action.payload}
    }

    case "RESET_SURVEY":{
      return {...state,  
                result : [], 
                survey: [], 
                surveyCount : 0, 
                forEdit: {},
                success: false,
                failed: false,
                error: null,
                fetching: false,
                fetched: false 
              }
      }

  
    default: {
      return {...state}
    }
  }
}