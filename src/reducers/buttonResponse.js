export default function reducer(state={}, action) {
  switch(action.type) {
    case "BUTTON_RESPONSE_CREATED": return action.payload
    case "RESET_BUTTON_RESPONSE": return {}
    default: return state
  }
}