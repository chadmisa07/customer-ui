import React from 'react'
import { IndexRoute, Route, Router, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import App from './components/app'
import Home from './components/home'
import Login from './components/login'
import Logout from './components/logout'
import FBAuth from './components/fbAuth'
import DashBoard from './components/main/customerUI/dashboard/dashboard'
import OrderManagement from './components/main/customerUI/dashboard/orderManagement'
import FAQManagement from './components/main/customerUI/dashboard/faqManagement'
import SurveyDetails from './components/main/customerUI/dashboard/surveyDetails'
import ControlPanel from './components/main/customerUI/controlPanel/controlPanel'
import CreateSurvey from './components/main/customerUI/createSurvey/create'
import store from './store/store'
import requireAuth from './utils/requireAuth'
import ReduxToastr from 'react-redux-toastr'
import Charts from './components/charts'
import HttpsRedirect from 'react-https-redirect'

const Routes = () => {
    return (
      <Provider store={store}>
        <HttpsRedirect>
          <div>
            <Router history={browserHistory}>
              <Route path="/" component={Login} />
              <Route path="/fb/*" component={FBAuth} />
              <Route path="/login" component={Login} />
              <Route path="/home" component={Home} />
              <Route path="/logout" component={Logout} />
              <Route path="/charts" component={Charts} />
              <Route path="/dashboard" component={requireAuth(App)}>
                <IndexRoute component={DashBoard} />
                <Route path="/dashboard" component={DashBoard} />
                <Route path="/dashboard/order-details" component={OrderManagement} />
                <Route path="/dashboard/survey-details" component={SurveyDetails} />
                <Route path="/dashboard/faq-details" component={FAQManagement} />
                <Route path="/create-survey" component={CreateSurvey} />
              </Route>
              <Route path="/control-panel" component={requireAuth(App)}>
                <IndexRoute component={ControlPanel} />
                <Route path="/control-panel" component={ControlPanel} />
                <Route path="/control-panel/create-survey" component={CreateSurvey} />
              </Route>
              <Route path="*" component={requireAuth(Logout)} />
            </Router>
            <ReduxToastr
              timeOut={5000}
              newestOnTop={true}
              preventDuplicates={false}
              position="top-right"
              transitionIn="fadeIn"
              transitionOut="fadeOut"
              progressBar/>
          </div>
        </HttpsRedirect>
      </Provider>
    )
}
export default Routes