import React from 'react'
import ReactDOM from 'react-dom'
import Routes from './routes'
import './includes/bootstrap/css/bootstrap.min.css'
import './includes/font-awesome/css/font-awesome.min.css'
import './includes/metisMenu/metisMenu.min.css'
import './includes/morrisjs/morris.css'
import './includes/dist/css/sb-admin-2.css'
import './includes/css/toastr.css'
import './includes/css/style.css'

ReactDOM.render(
	<Routes />, document.getElementById('root')
)
